#include <gmp.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <chrono>
#include <ctime>
#include <vector>

#define paillier_len 1233

using namespace std;


int main (int argc, const char * argv[]) {
	
	ifstream is("paillierTestInput");
	string line_a, line_b;
	mpz_t mp_nsquare, mp_a, mp_b, mp_result;
	mpz_init(mp_nsquare);
	mpz_init(mp_a);
	mpz_init(mp_b);
	mpz_init(mp_result);
	vector<double> times;

	// read nsquare
	getline(is, line_a);
	const char *nsquare = line_a.c_str();
	mpz_set_str(mp_nsquare, nsquare, 10);

	// read paillier ciphertexts, two at a time, and add each pair
	while (getline(is, line_a) && getline(is, line_b)) {
		const char *a = line_a.c_str();
		const char *b = line_b.c_str();
		mpz_set_str(mp_a, a, 10);
		mpz_set_str(mp_b, b, 10);

		// measure sum operation
		auto start = chrono::steady_clock::now();
		mpz_mul(mp_result, mp_a, mp_b);
		mpz_mod(mp_result, mp_result, mp_nsquare);
		auto end = chrono::steady_clock::now();
		double elapsed = double(chrono::duration_cast<chrono::nanoseconds>(end - start).count());
		times.push_back(elapsed / 1000000.0);
	}

	auto timeSum = 0.0;
	for (int i = 0; i < times.size(); i++)
		timeSum += times.at(i);
	auto mean = timeSum / times.size();

	// cleanup
	mpz_clear(mp_nsquare);
	mpz_clear(mp_a);
	mpz_clear(mp_b);
	mpz_clear(mp_result);
	
	cout << "Mean time of paillier Add operations: " << mean << " ms" << endl;

}
