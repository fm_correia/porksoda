/*
 * User Defined Functions for cryptographic operations.
 *
 * CryptDB code used as template available at:
 * https://github.com/CryptDB/cryptdb/blob/master/udf/edb.cc
 *
 * MySQL UDF documentation available at:
 * https://dev.mysql.com/doc/refman/5.7/en/adding-udf.html
 * */


#include <gmp.h>
#include <stdlib.h>
#include <string.h>
#include <mysql/mysql.h>
#include <ctype.h>
#include <stdio.h>

#define paillier_len 1233


/* ----- Sum of two encrypted integer values -------------------------------- */


my_bool crypto_sum_init (UDF_INIT *initid, UDF_ARGS *args, char *message) {

	if (args->arg_count != 3 || args->arg_type[0] != STRING_RESULT ||
			args->arg_type[1] != STRING_RESULT || args->arg_type[2] != STRING_RESULT) {
		strcpy(message, "usage: crypto_sum(string fieldname, string value, string key)");
		return 1;
	}

	return 0;

}


void crypto_sum_deinit (UDF_INIT *initid) {

	if (initid->ptr) {
		free(initid->ptr);
		initid->ptr = NULL;
	}

}


char *crypto_sum (UDF_INIT *initid, UDF_ARGS *args, char *result,
		unsigned long *length, char *is_null, char *error) {

	if (initid->ptr) {
		free(initid->ptr);
		initid->ptr = NULL;
	}

	mpz_t mp_result;
	mpz_init(mp_result);

	if (args->args[0] == NULL) {
		*length = 0;
		*is_null = 1;
	}
	else {
		/* declare and initialize MultiPrecision integer variables */
		mpz_t mp_a, mp_b, mp_nsquare;
		mpz_init(mp_a);
		mpz_init(mp_b);
		mpz_init(mp_nsquare);

		/*
		 * args->args buffers may be dirty from previous values
		 * if length of arg is lower than paillier_len, place null terminator
		 * */
		if (args->lengths[0] < paillier_len) {
			args->args[0][args->lengths[0]] = '\0';
		}
		if (args->lengths[1] < paillier_len) {
			args->args[1][args->lengths[1]] = '\0';
		}
		if (args->lengths[2] < paillier_len) {
			args->args[2][args->lengths[2]] = '\0';
		}

		/* set integer values from input parameters strings */
		mpz_set_str(mp_a, args->args[0], 10);
		mpz_set_str(mp_b, args->args[1], 10);
		mpz_set_str(mp_nsquare, args->args[2], 10);

		/* perform Paillier sum, store result in mp_result */
		mpz_mul(mp_result, mp_a, mp_b);
		mpz_mod(mp_result, mp_result, mp_nsquare);

		/* cleanup */
		mpz_clear(mp_a);
		mpz_clear(mp_b);
		mpz_clear(mp_nsquare);
	}

	int result_len = mpz_sizeinbase(mp_result, 10);
	char * buf = (char *) malloc(result_len);
	initid->ptr = buf;
	mpz_get_str(buf, 10, mp_result);
	*length = result_len;

	mpz_clear(mp_result);
	return initid->ptr;

}


/* ----- Aggregate sum ------------------------------------------------------ */


/* sum aggregator structure */
typedef struct agg_sum_state {
	mpz_t sum;			// sum accumulator variable
	mpz_t nsquare;		// paillier key
	int nsquare_set;	// is true if nsquare has been set
	char *buf;			// result buffer
} agg_sum_state_t;


my_bool crypto_agg_sum_init (UDF_INIT *initid, UDF_ARGS *args, char *message) {

	if (args->arg_count != 2 || args->arg_type[0] != STRING_RESULT ||
			args->arg_type[1] != STRING_RESULT) {
		strcpy(message, "usage: crypto_agg_sum(string fieldname, string key)");
		return 1;
	}

	agg_sum_state_t *state = malloc (sizeof(agg_sum_state_t));
	state->buf = malloc (paillier_len);
	mpz_init(state->sum);
	mpz_init(state->nsquare);
	initid->ptr = (char *) state;
	initid->maybe_null = 1;
	return 0;	

}


void crypto_agg_sum_deinit (UDF_INIT *initid) {

	agg_sum_state_t *state = (agg_sum_state_t *)(initid->ptr);
	mpz_clear(state->sum);
	mpz_clear(state->nsquare);
	free(state->buf);
	initid->ptr = NULL;

}


void crypto_agg_sum_clear (UDF_INIT *initid, char *is_null, char *error) {

	agg_sum_state_t *state = (agg_sum_state_t *)(initid->ptr);
	mpz_set_ui(state->sum, 1);
	state->nsquare_set = 0;

}


void crypto_agg_sum_add (UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error) {

	agg_sum_state_t *state = (agg_sum_state_t *)(initid->ptr);
	// set nsquare if not set yet
	if (!state->nsquare_set) {
		mpz_set_str(state->nsquare, args->args[1], 10);
		state->nsquare_set = 1;
	}

	// add current element to sum accumulator variable
	mpz_t curr;
	mpz_init(curr);
	fprintf(stderr, "%s\n", args->args[0]);
	if (args->args[0] == NULL) {
		mpz_set_ui(curr, 1);
	}
	else {
		if (args->lengths[0] < paillier_len) {
			args->args[0][args->lengths[0]] = '\0';
		}
		mpz_set_str(curr, args->args[0], 10);
	}
	mpz_mul(state->sum, state->sum, curr);
	mpz_mod(state->sum, state->sum, state->nsquare);
	mpz_clear(curr);

}


char *crypto_agg_sum (UDF_INIT *initid, UDF_ARGS *args, char *result,
		unsigned long *length, char *is_null, char *error) {

	agg_sum_state_t *state = (agg_sum_state_t *)(initid->ptr);
	int result_len = mpz_sizeinbase(state->sum, 10);
	//state->buf = (char *) malloc(result_len);
	mpz_get_str(state->buf, 10, state->sum);
	*length = result_len;
	return state->buf;

}


// EOF
