###Dependencies
`sudo apt-get install libgmp3-dev libmysqlclient-dev`


###Compiling and Installing
Run make to compile.

Place compiled library file (libcryptoFun.so) in mysql plugin directory `/usr/lib/mysql/plugin/`.

Login to mysql and create functions by executing:

```
CREATE FUNCTION crypto_sum RETURNS STRING SONAME 'libcryptoFun.so';
CREATE AGGREGATE FUNCTION crypto_agg_sum RETURNS STRING SONAME 'libcryptoFun.so';
```
