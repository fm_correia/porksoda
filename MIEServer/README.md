###Dependencies
`sudo apt-get install libssl-dev`

All other dependencies should already be satisfied if you've followed the instructions on `PorkSoda/README.md`.

###Compiling
Run `make`.

###Running the MIE Server
MIEServer stores data in folder `$HOME_DIR_CBIR/Data/Server/MIE`. This directory structure must exist. Select a path to be `$HOME_DIR_CBIR`, export it and create the directory structure.

Run the Server executable with argument "MIE":

```
export HOME_DIR_CBIR=/example/path/to/data/folder/
mkdir -p $HOME_DIR_CBIR/Data/Server/MIE
./Server MIE
```
