#PorkSoda
A Middleware solution for Multimodal SQL + Image queries on encrypted backends.

Thesis work developed at FCT-UNL by Filipe Correia under orientation of Prof. Dr. Henrique João Domingos.

###Directory contents
######PorkSoda
PorkSoda Middleware SBT project, with build instructions.

######udf
MySQL User Defined Functions shared library for Homomorphic operations support.

######MIEServer
Content Based Image Retrieval backend.

######smallImageDataset
A subset of the image dataset used in our benchmarks for Content Based Image Retrieval and Multimodal SQL + Image queries.

###Installing
Follow the instructions on PorkSoda/README.md
