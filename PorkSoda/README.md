##Description
This SBT project builds all three main components of the PorkSoda middleware into a single JAR file. The components are:

Client Proxy: handles SQL / MIE / Multimodal SQL + MIE requests from clients, encrypts all sensitive data and forwards the request to the Server Proxy.

Server Proxy: receives anonymized requests from the Client Proxy. Manages connections to MySQL and MIEServer, to which the received requests are dispatched. For Multimodal SQL + MIE requests, combines the results in a meaningful way.

Client: Provides API calls to perform all supported operations.


##Install
Before building the middleware with sbt, the common dependencies listed below are required. Machines running the Client / Server Proxies must also install specific dependencies.

The following instructions were tested on Ubuntu 16.04.

####Common dependencies
######Java 8, SBT
`sudo apt-get install openjdk-8-jdk`

```
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt-get update
sudo apt-get install sbt
```


####Client Proxy dependencies

######OpenCV 2.4.10
Download the OpenCV 2.4.10 Sources from [here](https://opencv.org/releases.html). It must be compiled from source with Java support (more on that further below).

OpenCV has "a few" dependencies of its own.

`sudo apt-get install build-essential cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev python-dev python-numpy libtbb-dev libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev libdc1394-22-dev ant`

Unpack OpenCV somewhere and cd to its directory, then build it:

```
sudo su
cd opencv-2.4.10; mkdir build; cd build
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
cmake -dwith_cuda=off -dwith_ffmpeg=off ..
make
```

Running `make install` is not necessary, in fact I advise against it if you're running both versions of OpenCV in the same machine.

####Server Proxy dependencies

######OpenCV 3.0.0
Download the OpenCV 3.0.0 Sources from [here](https://opencv.org/releases.html).

Dependencies are the same as those of OpenCV 2.4.10.

Unpack OpenCV somewhere and follow a similar procedure, except no Java support is necessary this time:

```
sudo su
cd opencv-3.0.0; mkdir build; cd build
cmake -dwith_cuda=off -dwith_ffmpeg=off ..
make
make install
```

For this OpenCV version make sure to run `make install` at the end.

######MySQL 5.7

`sudo apt-get install mysql-server`

After installing MySQL Server:

- Install the User Defined Functions shared library, necessary to support certain Homomorphic operations. See udf folder at repository root for instructions.

- Configure MySQL Server by editing `/etc/mysql/mysql.conf.d/mysqld.cnf`. Add the following lines under the `[mysqld]` section:

```
innodb_file_format = barracuda
innodb_file_per_table = 1
innodb_large_prefix = 1
```


######MIEServer
See MIEServer folder at repository root.


##Compiling and Running the middleware
Start by running `sbt` from the PorkSoda directory. On its first run, sbt will take some time to fetch all project dependencies. After it's done and the shell comes up, type `assembly`. Alternatively, you can run the `sbt assembly` shortcut from the command line. It will compile and package everything into a single JAR file, then found in `target/scala-2.12/`. Three .jar files will be generated, but the one with suffix "assembly-1.2.jar" is what we need.


####Running the Client Proxy
Note: if the Client Proxy cannot find a databaseSchemas file and a cbirFiles folder in the directory from which it is run, it will generate them. Thus, make sure to run it from the same path in subsequent runs.

Assuming OpenCV is found at the absolute path `/home/user/opencv-2.4.10/` and compiled with Java support, a JAR file can be found at `/home/user/opencv-2.4.10/build/bin/opencv-2410.jar`. 

`export LD_LIBRARY_PATH=/home/user/opencv-2.4.10/build/lib`

and then run the Client Proxy with:

`java -cp /home/user/opencv-2.4.10/build/bin/opencv-2.4.10.jar:path/to/PorkSoda-assembly-1.2.jar clientproxy.Main`


####Running the Server Proxy
Run the MIEServer if the Client will issue MIE or Multimodal SQL + MIE requests. If the Client only issues SQL queries this step can be ignored.

`java -cp path/to/PorkSoda-assembly-1.2.jar serverproxy.Main`


####Running the Client
Note: The directory from which a Client is run must contain the datasets folder.

There are two options to run the Client.

First option is to start up a Scala shell. Ubuntu repositories are stale, and for this we need at least version 2.12.3, so instead get Scala from [here](https://downloads.lightbend.com/scala/2.12.4/scala-2.12.4.deb). Install it `sudo dpkg -i scala-2.12.4.deb`, and done. Run scala with the JAR in the classpath: `scala -cp path/to/PorkSoda-assembly-1.2.jar`.

From there import the Client class, create a new instance and connect to the Client Proxy. The example below assumes the Client Proxy is running on the same machine:

```
import httpclient.Client
var client = new Client

// if database 'testDB' exists
// client.connect("localhost:5150", "testDB", "root", "password")
// empty string if no particular database is to be selected
// client.connect("localhost:5150", "", "root", "password")

client.connect("localhost:5150", "", "root", "password")
client.executeUpdate("create database if not exists testDB")
client.executeUpdate("use testDB")	// only needed because we connected with empty database string

val gerbils = Array(("Max", 2), ("Claws", 1), ("Capone", 4), ("Jim", 3), ("Chuck", 2), ("Jeeves", 2))
client.executeUpdate("create table gerbils (name VARCHAR(15), age INT)")
for (g <- gerbils)
	client.executeUpdate(s"insert into gerbils (name, age) values ('${g._1}', ${g._2})")

val rs = client.executeQuery("select name from gerbils where age < 4")
while (rs.next) println(rs.getString(1))

client.disconnect
```

More extended examples on how to use the Client class can be found at `src/main/scala/eval/`.

Second option is to write a client application that uses Client to make calls to the Client Proxy. Client applications can be written in either Scala or Java. For the latter there is no need to install Scala.


##Additional notes
If you need to import the project into:

Eclipse, a plugin is included for that purpose. Just run `eclipse` from the sbt shell, then use the Import Wizard in Eclipse to import "Existing Projects Into Workspace". An Eclipse version with Scala support can be found [here](http://scala-ide.org/download/sdk.html).

IntelliJ IDEA, instructions for a similar plugin can be found [here](https://github.com/JetBrains/sbt-idea-plugin), although I've not tested it.
