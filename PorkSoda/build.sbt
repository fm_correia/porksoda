name := "PorkSoda"

version := "1.2"

scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
	"com.typesafe.akka" %% "akka-http" % "10.0.10",
	"com.typesafe.akka" %% "akka-http-spray-json" % "10.0.10",
	"mysql" % "mysql-connector-java" % "5.1.24"
)

cancelable in Global := true

exportJars := true

mainClass in Compile := Some("clientproxy.Main")
