package httpclient

import akka.actor.{Actor, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.StatusCodes
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Attributes, StreamTcpException, OverflowStrategy}
import akka.util.ByteString
import akka.util.Timeout

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

import spray.json._
import akka.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.unmarshalling.Unmarshal

import akka.stream.scaladsl.{Source, Sink, Keep, SinkQueueWithCancel}
import akka.stream.scaladsl.Framing

import common._
import common.ProxyJsonSupport._
import common.Util

import java.sql.SQLException
import java.util.concurrent.TimeoutException



/*
 * Provides function calls similar to those of a ResultSet
 * */
class StreamedResultSet (sinkQueue: SinkQueueWithCancel[RSRow]) {

  import java.io.{ByteArrayInputStream, ObjectInputStream}
	import scala.concurrent.ExecutionContext.Implicits.global

  private final val timeout: Timeout = 10.seconds
  var currentFuture: Future[Option[RSRow]] = pull
  var current: RSRow = null

  private def pull: Future[Option[RSRow]] = {
    try {
      sinkQueue pull
    }
    catch {
      case e: Throwable =>
      Future(None)
    }
  }

  /*
   * * Haven't found how to cancel a stream properly...
   * * Must consume entire stream

   def cancel () = {
     sinkQueue.cancel
     while(next) {}
     }
   */

  def isLast: Boolean = {
    try {
      val result = Await.result(currentFuture, timeout.duration)
      result match {
        case Some(_) =>
          false
        case None =>
          true
      }
    }
    catch {
      case e: TimeoutException => return true
    }
  }

  def next: Boolean = {
    try {
      val result = Await.result(currentFuture, timeout.duration)
      result match {
        case Some(row: RSRow) =>
          current = row
          if (row.cols == null)
            return false
          currentFuture = pull
          true
        case None =>
          current = null
          false
      }
    }
    catch {
      case e: StreamTcpException => return false
      case _ => return false
    }
  }


  def getInt (colIndex: Int): Int = {
    val ois = new ObjectInputStream(new ByteArrayInputStream(current.cols(colIndex - 1)))
    val obj = ois.readObject
    ois.close
    obj.asInstanceOf[Int]
  }


  def getLong (colIndex: Int): Long = {
    val ois = new ObjectInputStream(new ByteArrayInputStream(current.cols(colIndex - 1)))
    val obj = ois.readObject
    ois.close
    obj.asInstanceOf[Long]
  }


  def getString (colIndex: Int): String = {
    val ois = new ObjectInputStream(new ByteArrayInputStream(current.cols(colIndex - 1)))
    val obj = ois.readObject
    ois.close
    obj.asInstanceOf[String]
  }


  def getObject (colIndex: Int): Object = {
    val ois = new ObjectInputStream(new ByteArrayInputStream(current.cols(colIndex - 1)))
    val obj = ois.readObject
    ois.close
    obj
  }

}


object Client {

  private final val timeout: Timeout = 360.seconds

}


class Client {

  import Client._

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  private var address: String = ""
  private var id: String = ""


  def connect (address: String, database: String, user: String, password: String) {
    val connect = SQLConnect(database, user, password)
    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = s"http://$address/connect",
      entity = Await.result(Marshal(connect).to[RequestEntity], timeout.duration))
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    id = Await.result(Unmarshal(response.entity).to[String], timeout.duration)
    this.address = address
  }


  def disconnect () {
    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = s"http://$address/disconnect?id=$id")
    Await.ready(Http().singleRequest(request), timeout.duration)
    address = ""
    id = ""
    system.terminate
  }


  @throws(classOf[SQLException])
  def setAutoCommit (autoCommit: Boolean) {
    val queryEntityFuture = Marshal(SQLSetAutoCommit(autoCommit)).to[RequestEntity]
    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = s"http://$address/setAutoCommit?id=$id",
      entity = Await.result(queryEntityFuture, timeout.duration))
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    response.status match {
      case StatusCodes.ImATeapot =>
        val failure = Await.result(Unmarshal(response.entity).to[SQLFailure], timeout.duration)
        throw new SQLException(failure.exceptionCause)
      case _ => response discardEntityBytes _
    }
  }


  @throws(classOf[SQLException])
  def setTransactionIsolation (level: Int) {
    val queryEntityFuture = Marshal(SQLSetTransactionIsolation(level)).to[RequestEntity]
    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = s"http://$address/setTransactionIsolation?id=$id",
      entity = Await.result(queryEntityFuture, timeout.duration))
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    response.status match {
      case StatusCodes.ImATeapot =>
        val failure = Await.result(Unmarshal(response.entity).to[SQLFailure], timeout.duration)
        throw new SQLException(failure.exceptionCause)
      case _ => response discardEntityBytes _
    }
  }


  @throws(classOf[SQLException])
  def commit () {
    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = s"http://$address/commit?id=$id")
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    response.status match {
      case StatusCodes.ImATeapot =>
        val failure = Await.result(Unmarshal(response.entity).to[SQLFailure], timeout.duration)
        throw new SQLException(failure.exceptionCause)
      case _ => response discardEntityBytes _
    }
  }


  @throws(classOf[SQLException])
  def rollback () {
    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = s"http://$address/rollback?id=$id")
    val response = Await.result(Http().singleRequest(request), timeout.duration) 
    response.status match {
      case StatusCodes.ImATeapot =>
        val failure = Await.result(Unmarshal(response.entity).to[SQLFailure], timeout.duration)
        throw new SQLException(failure.exceptionCause)
      case _ => response discardEntityBytes _
    }
  }


  @throws(classOf[SQLException])
  def executeUpdate (query: String): Int = {
    val queryEntityFuture = Marshal(SQLExecuteUpdate(query)).to[RequestEntity]
    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = s"http://$address/executeUpdate?id=$id",
      entity = Await.result(queryEntityFuture, timeout.duration))
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    response.status match {
      case StatusCodes.ImATeapot =>
        val failure = Await.result(Unmarshal(response.entity).to[SQLFailure], timeout.duration)
        throw new SQLException(failure.exceptionCause)
      case StatusCodes.OK =>
        val success = Await.result(Unmarshal(response.entity).to[SQLSuccess], timeout.duration)
        success.rows
			case s =>
				println(s)
				throw new SQLException("executeUpdate comm failure")
    }
  }


  @throws(classOf[SQLException])
  def executeQuery (query: String, nCols: Int): StreamedResultSet = {
    try {
      val queryEntityFuture = Marshal(SQLExecuteQuery(query, nCols)).to[RequestEntity]
      val request = HttpRequest(
        method = HttpMethods.POST,
        uri = s"http://$address/executeQuery?id=$id",
        entity = Await.result(queryEntityFuture, timeout.duration))
      
      val sink = Sink.queue[RSRow]().withAttributes(Attributes.inputBuffer(500, 500))
      val response = Await.result(Http().singleRequest(request), timeout.duration)
      response.status match {
        case StatusCodes.ImATeapot =>
          val failure = Await.result(Unmarshal(response.entity).to[SQLFailure], timeout.duration)
          throw new SQLException(failure.exceptionCause)
        case _ =>
          val sinkQueue = response.entity.dataBytes
            .buffer(2, OverflowStrategy.backpressure)
            .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = Int.MaxValue, allowTruncation = true))
            .mapAsync(1) (row => Unmarshal(row).to[RSRow])
            .toMat(sink)(Keep.right).run()
          new StreamedResultSet(sinkQueue)
      }
    }
    catch {
      case e: SQLException => throw e
    }
  }


  @throws(classOf[SQLException])
  def executeQuery (query: String): StreamedResultSet = {
    try {
      executeQuery(query, 0)
    }
    catch {
      case e: SQLException => throw e
    }
  }


  def indexImages: MIEResponse = {
    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = s"http://$address/indexImages?id=$id")
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    response.status match {
      case StatusCodes.ImATeapot =>
        val failure = Await.result(Unmarshal(response.entity).to[MIEFailure], timeout.duration)
        failure
      case _ =>
        val success = Await.result(Unmarshal(response.entity).to[MIESuccess], timeout.duration)
        success
    }
  }


  def addImage (imageId: Int, image: Array[Byte]): MIEResponse = {
    val requestEntityFuture = Marshal(MIEAddImage(imageId, image)).to[RequestEntity]
    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = s"http://$address/addImage?id=$id",
      entity = Await.result(requestEntityFuture, timeout.duration))
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    response.status match {
      case StatusCodes.ImATeapot =>
        val failure = Await.result(Unmarshal(response.entity).to[MIEFailure], timeout.duration)
        failure
      case _ =>
        val success = Await.result(Unmarshal(response.entity).to[MIESuccess], timeout.duration)
        success
    }
  }


  def addImage (dirPath: String, imageId: Int): MIEResponse = {
    val bytes = Util.readImg(dirPath, imageId)
    addImage(imageId, bytes)
  }


  def searchImage (bytes: Array[Byte]): MIESearchResultArray = {
    val requestEntityFuture = Marshal(MIESearchImage(bytes)).to[RequestEntity]
    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = s"http://$address/searchImage?id=$id",
      entity = Await.result(requestEntityFuture, timeout.duration))
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    response.status match {
      /*
      case StatusCodes.ImATeapot =>
        val failure = Await.result(Unmarshal(response.entity).to[MIEFailure], timeout.duration)
        failure
      */
      case _ =>
        val results = Await.result(Unmarshal(response.entity).to[MIESearchResultArray], timeout.duration)
        results
    }
  }


  def searchImage (dirPath: String, imageId: Int): MIESearchResultArray = {
    val bytes = Util.readImg(dirPath, imageId)
    searchImage(bytes)
  }


  def getImage (imageId: Int): MIEImage = {
    val requestEntityFuture = Marshal(MIEGetImage(imageId)).to[RequestEntity]
    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = s"http://$address/getImage?id=$id",
      entity = Await.result(requestEntityFuture, timeout.duration))
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    response.status match {
      case _ =>
      val mieImage = Await.result(Unmarshal(response.entity).to[MIEImage], timeout.duration)
      mieImage
    }
  }


  @throws(classOf[SQLException])
  def executeMultimodalQuery (sqlQuery: String, conjunction: String,
    imageField: String, bytes: Array[Byte], sqlQueryNumCols: Int = -1): StreamedResultSet = {

    val multimodalQuery = new MultimodalQuery(new SQLExecuteQuery(sqlQuery, sqlQueryNumCols), conjunction,
      imageField, new MIESearchImage(bytes))
    val requestEntityFuture = Marshal(multimodalQuery).to[RequestEntity]
    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = s"http://$address/multimodalQuery?id=$id",
      entity = Await.result(requestEntityFuture, timeout.duration))

    val sink = Sink.queue[RSRow]().withAttributes(Attributes.inputBuffer(1, 1))
    val response = Await.result(Http().singleRequest(request), timeout.duration)
    response.status match {
      case StatusCodes.ImATeapot =>
        val failure = Await.result(Unmarshal(response.entity).to[SQLFailure], timeout.duration)
        throw new SQLException(failure.exceptionCause)
      case _ =>
        val sinkQueue = response.entity.dataBytes
          .buffer(2, OverflowStrategy.backpressure)
          .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = Int.MaxValue, allowTruncation = true))
          .mapAsync(1) (row => Unmarshal(row).to[RSRow])
          .toMat(sink)(Keep.right).run()
        new StreamedResultSet(sinkQueue)
    }
  }


  @throws(classOf[SQLException])
  def executeMultimodalQuery (sqlQuery: String, conjunction: String, imageField: String, dirPath: String, imageId: Int): StreamedResultSet = {
    val bytes = Util.readImg(dirPath, imageId)
    executeMultimodalQuery(sqlQuery, conjunction, imageField, bytes)
  }


}
