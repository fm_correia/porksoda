package serverproxy

/*
 *
 * */


import akka.actor.{Actor, ActorSystem, ActorRef, Props, PoisonPill}
import scala.collection.mutable.Map

import common._


class ManagerActor extends Actor {
  
  // maps client proxy connection IDs to ActorRefs
  val refMap = scala.collection.mutable.Map[Int, ActorRef] ()
  // connections are assigned their IDs by the current ticket
  var ticket: Int = 0
  // who sent a Connect request
  var currentSender: ActorRef = null

	//implicit val executionContext = context.dispatchers.lookup("server-proxy-blocking-dispatcher")


  def receive = {
    
    /*
     * Spawn a RequestHandlerActor for the new connection and
     * issue the SQL connection request to it.
     * */
    case Connect (request: SQLConnect) =>
      currentSender = sender()
      val ref = context.actorOf(Props[RequestHandlerActor].withDispatcher("server-proxy-blocking-dispatcher"), name = "actor" + ticket)
      ref ! request   // issue SQL connection request to the new actor
      
    /*
     * Receive the connection status (successful or failed) for the last
     * issued request.
     * */
    case ConnectResponse (status: Boolean) =>
      if (status) {
        refMap += ((ticket, sender()))
        currentSender ! Some(ticket)   // return the assigned ID for the established connection
        ticket += 1
        currentSender = null
      }
      else {
        sender() ! PoisonPill  // connection failed, terminate the actor
        currentSender ! None
      }

    /*
     * Signal the connection's actor to close
     * the SQL connection and terminate the actor
     * */
    case Disconnect (id) =>
      (refMap get id) match {
        case Some(ref)  => ref ! PoisonPill  // goodnight sweet prince :(
        case None       => println("Disconnect: Actor not found")
      }
      refMap -= id

    /*
     * Forward a Request message from a client to the
     * respective RequestHandlerActor
     * */
    case Forward (id, request: Request) =>
      (refMap get id) match {
        // forward is used here to maintain the original sender
        case Some (ref) => ref forward request
        case None       => println("Forward: Actor not found")
      }

  }

}
