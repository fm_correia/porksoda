package serverproxy

/*
 * The RequestHandlerActor handles requests for SQL/MIE/SQL+MIE operations.
 * */


import akka.actor.{Actor, ActorSystem, ActorRef, Props}
import akka.stream.ActorAttributes
import akka.stream.scaladsl.{Source, Sink}
import java.sql.{ResultSet, SQLException}
import akka.util.Timeout

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

import common._
import common.DatabaseConnector
import cbir.MIEServerConnector

import java.io.{ByteArrayOutputStream, ObjectOutputStream}




class RequestHandlerActor extends Actor {

  val timeout: Timeout = 6.minutes

  //implicit val executionContext = context.system.dispatchers.lookup("server-proxy-single-thread")
  var dc: DatabaseConnector = new DatabaseConnector
  var mieConn: MIEServerConnector = new MIEServerConnector

	//implicit val executionContext = system.dispatchers.lookup("server-proxy-blocking-dispatcher")


  override def postStop() {
    if (dc != null)
      dc.closeConnection()
    dc = null
  }


  def getResultSetRow (rs: ResultSet, nCols: Int): RSRow = {
    if (rs != null && rs.next()) {
      RSRow(for (i <- 1 to nCols) yield {
        val bas = new ByteArrayOutputStream
        val oos = new ObjectOutputStream(bas)
        oos.writeObject(rs.getObject(i))
        val bytes = bas.toByteArray
        oos.close
        bytes
      })
    }
    else {
      null
    }
  }


  def receive = {
  
    /*
     * start the SQL connection to the required database
     * TODO: provide a return message for both success and failure
     * */
    case SQLConnect(db, user, password) =>
      try {
        dc.establishConnection(db, user, password)
        sender() ! ConnectResponse(true)
      }
      catch {
        case e: SQLException => sender() ! ConnectResponse(false)
      }


    /*
     *
     * */
    case SQLSetAutoCommit(autoCommit) =>
      try {
        dc.setAutoCommit(autoCommit)
        sender() ! SQLSuccess(0)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLSetTransactionIsolation(level) =>
      try {
        dc.setTransactionIsolation(level)
        sender() ! SQLSuccess(0)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLCommit =>
      try {
        dc.commit
        sender() ! SQLSuccess(0)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLRollback =>
      try {
        dc.rollback
        sender() ! SQLSuccess(0)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLExecuteUpdate(query) =>
      try {
        val updateResult = dc.executeUpdate(query)
        sender() ! SQLSuccess(updateResult)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLExecuteQuery(query, nCols) =>
      try {
        val source = Source.unfoldResource[RSRow, ResultSet](() => dc.executeQuery(query),
          rs => Option(getResultSetRow(rs, nCols)),
          rs => rs.close)
        sender() ! Left(source)
      }
      catch {
        case e: SQLException => sender() ! Right(SQLFailure(e.toString))
      }


    /*
     *
     * */
    case MIEIndex =>
      mieConn.index
      sender() ! MIESuccess("Indexing started")


    /*
     *
     * */
    case MIEAddImage (id: Int, bytes: Array[Byte]) =>
      // id not needed
      mieConn.addImage(bytes)
      sender() ! MIESuccess("Image added to repository")


    /*
     *
     * */
    case MIESearchImage (bytes: Array[Byte]) =>
      val searchResults = mieConn.search(bytes)
      sender() ! MIESearchResultArray(searchResults)


    /*
     *
     * */
    case MIEGetImage (id: Int) =>
      val image = mieConn.getImage(id)
      sender() ! image
  	    

    /*
     *
     * */
    case MultimodalQuery (sqlQuery: SQLExecuteQuery, conjunction: String, imageField: String, mieSearch: MIESearchImage) =>
      try {
        /*
         * Sub optimal approach: linearize queries
         * Perform MIE search first, add WHERE clause to SQL query.
         * */
				
        val mieSearchResults = mieConn.search(mieSearch.bytes)
        val imageClause = s"$imageField in " + (for (i <- mieSearchResults) yield i.id).mkString("(", ", ", ")")
        var sqlQueryWithImageClause = sqlQuery.query

        conjunction.toUpperCase match {
				  case "AND" =>
            sqlQueryWithImageClause ++= s" AND $imageClause"
          case "OR" =>
            sqlQueryWithImageClause ++= s" OR $imageClause"
          case "SINGLECLAUSE" =>
            sqlQueryWithImageClause ++= s" WHERE $imageClause"
        }
        //println(sqlQueryWithImageClause)

        val source = Source.unfoldResource[RSRow, ResultSet](() => dc.executeQuery(sqlQueryWithImageClause),
          rs => Option(getResultSetRow(rs, sqlQuery.nCols)),
          rs => rs.close)
        sender() ! Left(source)
      }
      catch {
        case e: SQLException => sender() ! Right(SQLFailure(e.toString))
      }
  	}
  
}
