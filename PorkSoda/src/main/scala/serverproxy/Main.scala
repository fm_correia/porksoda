package serverproxy

import akka.actor.{Actor, ActorSystem, ActorRef, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.stream.ActorMaterializer
import akka.pattern.ask
import akka.stream.scaladsl.{Source, Flow, Sink}
import scala.io.StdIn
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import akka.util.Timeout
import akka.util.ByteString

import common._
import common.ProxyJsonSupport._


object Main {

  def main (args: Array[String]) {
  
    var address = "localhost"
    var port = 6505

    if (args.size == 2) {
      address = args(0)
      port = args(1).toInt
    }
    else if (args.size > 2) {
      println("usage: serverproxy.Main [address port]\nExample: serverproxy.Main localhost 6505")
      System.exit(1)
    }


    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()

    // dedicated dispatcher for blocking operations
    implicit val executionContext = system.dispatchers.lookup("server-proxy-blocking-dispatcher")

    implicit val timeout: Timeout = 60.seconds

    val start = ByteString.empty
    val sep = ByteString("\n")
    val end = ByteString.empty
    implicit val jsonStreamingSupport: JsonEntityStreamingSupport = EntityStreamingSupport.json()
      .withFramingRenderer(Flow[ByteString].intersperse(start, sep, end))

    // handles ClientProxy sessions and request forwarding
    val manager = system.actorOf(Props[ManagerActor].withDispatcher("server-proxy-blocking-dispatcher"), "manager")

    val route =
      path("connect") {
        post {
          entity(as[SQLConnect]) { request =>
            onSuccess((manager ? Connect(request)).mapTo[Option[Int]]) {
              case Some(id) =>
                println(s"Connection established with ClientProxy for Client #$id.")
                complete(StatusCodes.OK, s"$id")
              case None =>
                complete(StatusCodes.InternalServerError)
            }
          }
        }
      } ~
      path("disconnect") {
        get {
          parameter("id".as[Int]) { id =>
            manager ! Disconnect(id)
            println(s"Connection terminated for Client #$id.")
            complete(StatusCodes.OK, "disconnected")
          }
        }
      } ~
      path("setAutoCommit") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[SQLSetAutoCommit]) { setAutoCommit =>
              onSuccess((manager ? Forward(id, setAutoCommit)).mapTo[SQLResponse]) {
                case SQLSuccess(_) => complete(StatusCodes.OK)
                case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
              }
            }
          }
        }
      } ~
      path("setTransactionIsolation") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[SQLSetTransactionIsolation]) { setTransactionIsolation =>
              onSuccess((manager ? Forward(id, setTransactionIsolation)).mapTo[SQLResponse]) {
                case SQLSuccess(_) => complete(StatusCodes.OK)
                case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
              }
            }
          }
        }
      } ~
      path("commit") {
        get {
          parameter("id".as[Int]) { id =>
            onSuccess((manager ? Forward(id, SQLCommit)).mapTo[SQLResponse]) {
              case SQLSuccess(_) => complete(StatusCodes.OK)
              case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
            }
          }
        }
      } ~
      path("rollback") {
        get {
          parameter("id".as[Int]) { id =>
            onSuccess((manager ? Forward(id, SQLRollback)).mapTo[SQLResponse]) {
              case SQLSuccess(_) => complete(StatusCodes.OK)
              case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
            }
          }
        }
      } ~
      path("executeUpdate") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[SQLExecuteUpdate]) { query =>
              onSuccess((manager ? Forward(id, query)).mapTo[SQLResponse]) {
                case s: SQLSuccess => complete(StatusCodes.OK, s)
                case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
              }
            }
          }
        }
      } ~
      path("executeQuery") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[SQLExecuteQuery]) { query =>
              onSuccess((manager ? Forward(id, query)).mapTo[Either[Source[RSRow, _], SQLFailure]]) {
                case Left(source) => complete(source)
                case Right(failure) => complete(StatusCodes.ImATeapot, failure)
              }
            }
          }
        }
      } ~
      path("indexImages") {
        get {
          parameter("id".as[Int]) { id =>
            val future = manager ? Forward(id, MIEIndex)
            val futureResult = Await.result(future, timeout.duration).asInstanceOf[MIEResponse]
            futureResult match {
              case s: MIESuccess => complete(s)
              case failure: MIEFailure => complete(StatusCodes.ImATeapot, failure)
            }
          }
        }
      } ~
      path("addImage") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[MIEAddImage]) { request =>
              val future = manager ? Forward(id, request)
              val futureResult = Await.result(future, timeout.duration).asInstanceOf[MIEResponse]
              futureResult match {
                case s: MIESuccess => complete(s)
                case failure: MIEFailure => complete(StatusCodes.ImATeapot, failure)
              }
            }
          }
        }
      } ~
      path("searchImage") {
        post {
           parameter("id".as[Int]) { id =>
            entity(as[MIESearchImage]) { request =>
              val future = manager ? Forward(id, request)
              val futureResult = Await.result(future, timeout.duration).asInstanceOf[MIEData]
              futureResult match {
                case searchResults: MIESearchResultArray => complete(searchResults)
              }
            }
          }       
        }
      } ~
      path("getImage") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[MIEGetImage]) { request =>
              val future = manager ? Forward(id, request)
              val futureResult = Await.result(future, timeout.duration).asInstanceOf[MIEImage]
              futureResult match {
                case image: MIEImage => complete(image)
              }
            }
          }
        }
      } ~
      path("multimodalQuery") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[MultimodalQuery]) { query =>
              onSuccess(Future {
                val future = manager ? Forward(id, query)
                Await.result(future, timeout.duration).asInstanceOf[Either[Source[RSRow, _], SQLFailure]]
              }) { sourceFuture =>
                sourceFuture match {
                  case Left(source) => complete(source)
                  case Right(failure) => complete(StatusCodes.ImATeapot, failure)
                }
              }
            }
          }
        }
      }



      /*
       * Bind the proxy at http://address:port/
       * Trigger unbind and termination if RETURN is pressed
       * */
      val bindingFuture = Http().bindAndHandle(route, address, port)
      println(s"Server Proxy online at http://$address:$port/\nPress RETURN to stop...")
      StdIn.readLine()

      bindingFuture
        .flatMap(_.unbind())
        .onComplete(_ => system.terminate())
  
  }

}
