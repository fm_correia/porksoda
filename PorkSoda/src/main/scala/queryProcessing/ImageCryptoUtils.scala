package queryProcessing

import cbir.Crypto
import mie.utils.CBIRCipherText

import java.util.{ArrayList, LinkedList, List, Queue}
import java.util.zip.{DataFormatException, Deflater, Inflater}
import java.nio.ByteBuffer


object ImageCryptoUtils {

  private final val KeywordsSize: Int = 0
  private final val NumKeywords: Int = 0
  private final val TextCiphertextSize: Int = 0

}



class ImageCryptoUtils {

  import ImageCryptoUtils._
  
  var mieCrypto: Crypto = new Crypto


  /*
   *
   * */
  def zip (bytes: Array[Byte]): Array[Byte] = {
    val compressor = new Deflater
    compressor.setInput(bytes)
    compressor.finish
    val tmp = new Array[Byte](500)
    val buffer: Queue[Byte] = new LinkedList[Byte]
    
    var compressed: Int = compressor.deflate(tmp)
    while (compressed > 0) {
      for (i <- tmp)
        buffer.add(i)
      compressed = compressor.deflate(tmp)
    }

    val ret = new Array[Byte](buffer.size + 16)
    val bb = ByteBuffer.wrap(ret, 0, 16)
    bb.putLong(buffer.size)
    bb.putLong(bytes.size)
    val done = buffer.size
    for (i <- 16 to (done + 15))
      ret(i) = buffer.remove
    ret
  }


  /*
   *
   * */
  def unzip (zip: Array[Byte], dataSize: Int): Array[Byte] = {
    val decompressor = new Inflater
    decompressor.setInput(zip)
    try {
      val data = new Array[Byte](dataSize)
      decompressor.inflate(data)
      decompressor.end
      data
    }
    catch {
      case e: DataFormatException =>
        e.printStackTrace
        return null
    }
  }


  /*
   * Encrypts an image and respective features.
   * Returns an array of bytes with the image, features and metadata.
   * */
  def encryptImage (id: Int, image: Array[Byte]): Array[Byte] = {
    val ciphertextBytes = mieCrypto.encryptImage(image)
    val ciphertext = new CBIRCipherText(ciphertextBytes)
    val cbirLen = ciphertext.getCBIRLength
    val imageCiphertext = ciphertext.getCipherText  // encrypted image
    val imageFeatures = ciphertext.getImgFeatures   // encrypted features

    val numFeatures = if (imageFeatures != null) imageFeatures.size else 0
    val featuresSize =
      if (imageFeatures.size > 0)
        if (imageFeatures(0).size > 0) imageFeatures(0).size else 0
      else 0

    // prepare buffer for data
    val buffer = ByteBuffer.allocate(7 * 4 + cbirLen + imageCiphertext.size)
    // metadata
    buffer.putInt(id)
    buffer.putInt(numFeatures)
    buffer.putInt(featuresSize)
    buffer.putInt(NumKeywords)
    buffer.putInt(KeywordsSize)
    // ciphertext lengths
    buffer.putInt(imageCiphertext.size)
    buffer.putInt(TextCiphertextSize)  // not used
    // buffer image ciphertext
    buffer.put(imageCiphertext)
    // buffer image features
    for (i <- 0 to numFeatures - 1)
      for (j <- 0 to featuresSize - 1)
        buffer.putInt(imageFeatures(i)(j).asInstanceOf[Int])

    zip(buffer.array)
  }


  /*
   * Uncompress a zipped image and decrypt it
   * */
  def decryptImage (zippedBytes: Array[Byte], unzippedDatasize: Int): Array[Byte] = {
    // unzip data
    var data = unzip(zippedBytes, unzippedDatasize)
    var bb = ByteBuffer.wrap(data, 0, 8)
    val imageSize = bb.getInt
    // next getInt would get text size
    var encryptedImageBytes = new Array[Byte](imageSize)
    Array.copy(data, 8, encryptedImageBytes, 0, imageSize)

    // decrypt data
    mieCrypto.decryptImage(encryptedImageBytes)
  }


  /*
   * Obtain image features.
   * Returns an array of bytes with the features and metadata.
   * */
  def getImageFeatures (image: Array[Byte]): Array[Byte] = {
    val featureBytes = mieCrypto.getImageFeatures(image)
    val ciphertext = new CBIRCipherText(featureBytes)
    val features = ciphertext.getImgFeatures
    val numFeatures = features.size
    val featuresSize =
      if (features.size > 0)
        if (features(0).size > 0) features(0).size else 0
      else 0

    // prepare buffer for data
    val buffer = ByteBuffer.allocate(5 * 4 + ciphertext.getCBIRLength)
    // metadata
    buffer.putInt(0)  // no id
    buffer.putInt(numFeatures)
    buffer.putInt(featuresSize)
    buffer.putInt(NumKeywords)
    buffer.putInt(KeywordsSize)
    // buffer image features
    for (i <- 0 to numFeatures - 1)
      for (j <- 0 to featuresSize - 1)
        buffer.putInt(features(i)(j).asInstanceOf[Int])

    zip(buffer.array)
  }


}
