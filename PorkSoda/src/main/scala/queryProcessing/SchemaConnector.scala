package queryProcessing

import java.io.File
import java.sql.{Connection, DriverManager, ResultSet, SQLException, Statement}


/* -------------------------------------------------------------------------- */


object SchemaConnector {

  val driver = "org.sqlite.JDBC"
  val urlPrefix = "jdbc:sqlite:"

  /* Field names in the schema database */
  val schemaDatatypeString = "datatype"
  val schemaFieldNameString = "fieldName"

}


/* -------------------------------------------------------------------------- */


class SchemaConnector {


  import SchemaConnector._

  var connection: Connection = null
  var statement: Statement = null
  Class.forName(driver)


  /*
   * Check if the database schema with the given name exists.
   * */
  def checkSchemaExists (schemaDatabaseName: String): Boolean = {
    val schemaFile: File = new File(s"$schemaDatabaseName.db")
    schemaFile.exists
  }


  /*
   * Open a connection to the SQLite database schema with the given name.
   * Instantiate the Statement for posterior use.
   * */
  @throws(classOf[SQLException])
  def establishSchemaConnection (schemaDatabaseName: String) {
    try {
      connection = DriverManager.getConnection(s"$urlPrefix$schemaDatabaseName.db")
      statement = connection.createStatement()
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Close the current connection.
   * */
  @throws(classOf[SQLException])
  def closeSchemaConnection () {
    try {
      statement = null
      connection.close()
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Delete the database schema with the given name
   * */
  def deleteSchemaDatabase (schemaDatabaseName: String) {
    if (connection != null)
      closeSchemaConnection
    val schemaFile = new File(s"$schemaDatabaseName.db")
    schemaFile.delete
  }
  

  /*
   * Create a new table with the given anonymized name in the schema database.
   * */
  @throws(classOf[SQLException])
  def createSchemaTable (tableName: String) = {
    try {
      statement.executeUpdate(
        s"CREATE TABLE IF NOT EXISTS `$tableName` " +
        s"($schemaFieldNameString STRING, $schemaDatatypeString STRING)")
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Delete a table with the given anonymized name from the schema database.
   * */
  @throws(classOf[SQLException])
  def deleteSchemaTable (tableName: String) = {
    try {
      statement.executeUpdate(
        s"DROP TABLE IF EXISTS `$tableName`")
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Given an Array of (fieldName, datatype) pairs, insert them into
   * the schema table identified by the anonymized tableName.
   * */
  @throws(classOf[SQLException])
  def addFieldsToSchemaTable (tableName: String, fieldPairs: Array[(String, String)]) = {
    try {
      val insertString = 
        s"INSERT INTO `$tableName` ($schemaFieldNameString, $schemaDatatypeString) values " +
        (fieldPairs map (p => "'" + p._1 + "', '" + p._2 + "'")
        mkString ("(", "), (", ")"))
      statement.executeUpdate(insertString)
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Obtain the original datatype of an anonymized field on an anonymized table.
   * Returns the datatype name, or null if the field is not found.
   * */
  @throws(classOf[SQLException])
  def getFieldDatatype (tableName: String, fieldName: String): String = {
    try {
      val rs = statement.executeQuery(
        s"select $schemaDatatypeString from `$tableName` " +
        s"where $schemaFieldNameString = '$fieldName'")
      if (rs.next())
        rs.getString(schemaDatatypeString)
      else
        throw new SQLException(s"Field name `$fieldName` does not exist in table `$tableName`")
    }
    catch {
      case e: SQLException => throw e
    }
  }


}
