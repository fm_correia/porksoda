package queryProcessing

import scala.util.matching.Regex
import java.io.File
import java.sql.{ResultSet, SQLException}
import common._
import QueryCryptoUtils._

import httpclient.Client
import httpclient.StreamedResultSet


class ResultSetRowIterator (rs: StreamedResultSet,
    qc: QueryCryptoUtils, colSchemesTypes: (Array[Scheme], Array[String])) extends Iterator[RSRow] {

  import java.io.{ByteArrayOutputStream, ObjectOutputStream}

  val nCols = colSchemesTypes._1.size


  override def hasNext(): Boolean = {
    if (rs.isLast) {
      //rs.close
      false
    }
    else
      true
  }

  override def next(): RSRow = {
    rs.next
    RSRow(for (i <- 1 to nCols) yield {
        val valueInfo = (colSchemesTypes._1(i-1), colSchemesTypes._2(i-1))
        val plaintextObject = qc.decryptValue(rs.getObject(i), valueInfo)
        val bas = new ByteArrayOutputStream
        val oos = new ObjectOutputStream(bas)
        oos.writeObject(plaintextObject)
        val bytes = bas.toByteArray
        oos.close
        bytes
      })
  }

}


object QueryProcessor {

  final val useDatabaseRegex = "(?i)^(USE )(\\S+)$".r
  final val createDatabaseRegex = "(?i)^(\\bCREATE DATABASE\\b (?:\\bIF NOT EXISTS\\b )?)(\\S+)$".r
  final val dropDatabaseRegex = "(?i)^(\\bDROP DATABASE\\b (?:\\bIF EXISTS\\b )?)(\\S+)$".r
  final val createTableRegex = "(?i)^(\\bCREATE TABLE\\b (?:\\bIF NOT EXISTS\\b )?)(\\S+) \\((\\S+(?: \\S+)*)\\)$".r
  final val dropTableRegex = "(?i)^(\\bDROP TABLE\\b (?:\\bIF EXISTS\\b )?)(\\S+)$".r
  final val insertIntoRegex = "(?i)^(\\bINSERT INTO\\b )(\\S+) \\((\\S+(?: \\S+)*)\\)( \\bVALUES\\b )\\((\\S+(?: \\S+)*)\\)$".r
  final val updateSetRegex = "(?i)^(\\bUPDATE\\b) (\\S+) (\\bSET\\b) (.+?(?=(?: \\bWHERE\\b|$)))( \\bWHERE\\b )?(.+)?$".r
  final val deleteRegex = "(?i)^(\\bDELETE FROM\\b )(\\S+)( \\bWHERE\\b )?(.+)?$".r
  final val selectRegex = "(?i)^(\\bSELECT\\b(?: DISTINCT)?)( \\bFOR UPDATE\\b)? (.+?(?=(?: \\bFROM\\b)))( \\bFROM\\b )(.+?(?=(?: \\bWHERE\\b|$)))( \\bWHERE\\b )?(.+?(?=(?: \\bORDER BY\\b|$)))?( \\bORDER BY\\b )?(.+)?$".r
  final val datatypeRegex = "(?i)^(\\w+)(?:\\((\\d+)\\))?$".r
  final val updateAssignmentRegex = "(?i)^(\\S+) = (\\S+) ?(\\+|\\-)? ?(\\S+)?$".r
  final val conditionRegex = "(?i)^(\\S+) (<=|>=|<|>|=) (\\S+)( AND| OR)?$".r
  final val tableJoinRegex = "(?i)^(\\S+) (\\bJOIN\\b) (\\S+) (\\bON\\b) (.+)$".r
  
  final val countSelectionRegex = "(?i)^(\\bCOUNT\\b)\\((\\S+)\\)$".r
  final val coalesceSelectionRegex = "(?i)^(\\bCOALESCE\\b)\\((\\S+,\\S+)\\)$".r
  final val maxMinSelectionRegex = "(?i)^(\\bMAX\\b|\\bMIN\\b)\\((\\S+)\\)$".r
  final val sumSelectionRegex = "(?i)^(\\bSUM\\b)\\((\\S+)\\)$".r

  final val imageSelectionRegex = "(?i)^(\\bIMG\\b)\\((\\S+)\\)$".r

}


/* -------------------------------------------------------------------------- */


class QueryProcessor (databaseName: String, user: String, password: String) {


  import QueryProcessor._

  //var sc: SchemaConnector = new SchemaConnector
  var sc: Schema = new Schema
  var qc: QueryCryptoUtils = new QueryCryptoUtils
  var ic: ImageCryptoUtils = new ImageCryptoUtils
  var client: Client = new Client
  var rs: StreamedResultSet = null

	//val serverAddress = "34.241.33.1:6505"
	val serverAddress = "localhost:6505"

  var anonDatabaseName = ""
  if (databaseName != "") {
    val anonDbName = qc.anonymizeName(databaseName)
    if (sc.checkSchemaExists(anonDbName)) {
      //sc.establishSchemaConnection(anonDbName)
      anonDatabaseName = anonDbName
    }
  }
  client.connect(serverAddress, anonDatabaseName, user, password)


  @throws(classOf[SQLException])
  def close () {
    try {
      //sc.closeSchemaConnection()
      client.disconnect
			/*
      if (rs != null)
        rs.cancel
			*/
    }
    catch {
      case e: SQLException => throw e
    }
  }


  @throws(classOf[SQLException])
  def setAutoCommit (autoCommit: Boolean) {
    try {
      client.setAutoCommit(autoCommit)
    }
    catch {
      case e: SQLException => throw e
    }
  }


  @throws(classOf[SQLException])
  def setTransactionIsolation (level: Int) {
    try {
      client.setTransactionIsolation(level)
    }
    catch {
      case e: SQLException => throw e
    }
  }


  @throws(classOf[SQLException])
  def commit () {
    try {
      client.commit
    }
    catch {
      case e: SQLException => throw e
    }
  } 


  @throws(classOf[SQLException])
  def rollback () {
    try {
      client.rollback
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   *
   * */
  def translateWhereConditions (anonNamesMap: Map[String, String], conditions: String): String = {
    val conditionArray = (conditions split " ").sliding(4,4).toArray
    conditionArray map { c =>
      var anonTableName = ""
      var anonFieldName = ""
      var anonValue = ""
      if (anonNamesMap != null && anonNamesMap.size > 1) {
        /* fieldNames have format "tableName.fieldName" and anonNamesMap already contains
         * encryptions of tableNames */
        val fullName = c(0) split '.'
        anonTableName = s"`${anonNamesMap(fullName(0))}`."
        anonFieldName = qc anonymizeName fullName(1)
      }
      else {
        /* fieldNames are not prefixed by a table name */
        anonFieldName = qc anonymizeName c(0)
      }
      if (c(1) == "=") {
      // equality comparison (=)
        if ((c(2) startsWith "'") && (c(2) endsWith "'")) {
        // text value, encrypt with DET
          anonFieldName = qc translateDet anonFieldName
          anonValue = "'" + (qc encryptTextDet (c(2) stripPrefix "'" stripSuffix "'")) + "'"
        }
        else {
        // numeric value, encrypt with OPE
          anonFieldName = qc translateOpe anonFieldName
					if (c(2).toLowerCase == "null") c(2)
					else anonValue = (qc encryptOpe c(2).toInt).toString
        }
      }
      else {
      // inequality comparison (>=, <=, < or >), encrypt with OPE
        anonFieldName = qc translateOpe anonFieldName
				if (c(2).toLowerCase == "null") c(2)
        else anonValue = (qc encryptOpe c(2).toInt).toString
      }
      anonFieldName = anonTableName + anonFieldName
      if (c.length == 3)
        s"$anonFieldName ${c(1)} $anonValue"
      else
        s"$anonFieldName ${c(1)} $anonValue ${c(3)}"
    } mkString " "
  }


  /*
   * Given previously anonymized table and field names, and WHERE conditions, issue a
   * SELECT operation and return the ResultSet.
   * */
  @throws(classOf[SQLException])
  def selectStaleValues (anonTableName: String, fieldNames: Array[String], whereConditions: String): StreamedResultSet = {
    try {
      val fieldSelection = fieldNames mkString ", "
      if (whereConditions.length == 0)
        client.executeQuery(s"SELECT $fieldSelection FROM `$anonTableName`", fieldNames.size)
      else
	client.executeQuery(s"SELECT $fieldSelection FROM `$anonTableName` WHERE $whereConditions", fieldNames.size)
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Process and execute a query that does not return a ResultSet
   * */
  @throws(classOf[SQLException])
  def executeUpdate (query: String): Int = query match {
  
    case useDatabaseRegex(useKeyword, dbName) =>
      val anonDbName = qc anonymizeName dbName
      if (sc.checkSchemaExists(anonDbName)) {
        try {
          val res = client.executeUpdate(s"$useKeyword`$anonDbName`")   // issue the USE statement
          //sc.establishSchemaConnection(anonDbName)      // connect to the schema database
          anonDatabaseName = anonDbName   // update the database name variable in this class
          res
        }
        catch {
          case e: SQLException => throw e
        }
      }
      else 0
    

    case createDatabaseRegex(createKeyword, dbName) =>
      val anonDbName = qc anonymizeName dbName
      try {
        //sc.establishSchemaConnection(anonDbName)        // creates a new schema database
        sc.createDatabaseSchema(anonDbName)
        client.executeUpdate(s"$createKeyword`$anonDbName`")  // issue the CREATE DATABASE statement
      }
      catch {
        case e: SQLException => throw e
      }


    case dropDatabaseRegex(dropKeyword, dbName) =>
      val anonDbName = qc anonymizeName dbName
      try {
        val res = client.executeUpdate(s"$dropKeyword`$anonDbName`")    // issue the DROP DATABASE statement
        sc.deleteDatabaseSchema(anonDbName)             // delete the schema database
        res
      }
      catch {
        case e: SQLException => throw e
      }


    case createTableRegex(createKeyword, tableName, colDefinitions) =>
      val anonTableName = qc anonymizeName tableName
      val declarationArray = colDefinitions split ", "
      var anonFieldnameDatatypePairs: Array[(String, String)] = Array.empty // for later issuing the schema insert
      // process and translate each field declaration
      val anonDeclarationArray = declarationArray flatMap { d =>
        // curr(0) is the field name, curr(1) the datatype
        val curr = d split " " filter (_ != "")
        val translatedDeclarations = curr(1) match {  // match datatype regex
          case datatypeRegex(datatype, lengthParam) => {
            val anonFieldName = qc.anonymizeName(curr(0))
            anonFieldnameDatatypePairs :+= (anonFieldName, datatype)
            if (lengthParam == null)
              qc.translateDeclaration(anonFieldName, datatype)
            else
              qc.translateDeclaration(anonFieldName, datatype, lengthParam.toInt)
          }
        }
        /* if the original declaration had extra options such as "NOT NULL",
         * append them to its corresponding translated declarations */
        val rest = if (curr.length > 2) (curr drop 2).mkString(" ", " ", "") else ""
        translatedDeclarations map (_ + rest)
      }
      val anonDeclarations = anonDeclarationArray mkString ", "
      //println(s"$createKeyword`$anonTableName` ($anonDeclarations)")
      try {
        val res = client.executeUpdate(s"$createKeyword`$anonTableName` ($anonDeclarations) ROW_FORMAT = COMPRESSED") // issue the CREATE TABLE statement
        sc.createTableSchema(anonDatabaseName, anonTableName)   // add the new table to the schema database
        sc.insertFields(anonDatabaseName, anonTableName, anonFieldnameDatatypePairs)   // add the fields to the schema table
        res
      }
      catch {
        case e: SQLException => throw e
      }

 
    case dropTableRegex(dropKeyword, tableName) =>
      val anonTableName = qc anonymizeName tableName
      //println(s"$dropKeyword`$anonTableName`")
      try {
        val res = client.executeUpdate(s"$dropKeyword`$anonTableName`") // issue the DROP statement
        sc.deleteTableSchema(anonDatabaseName, anonTableName)               // drop the table from the schema database
        res
      }
      catch {
        case e: SQLException => throw e
      }


    case insertIntoRegex(insertKeyword, tableName, fieldNames, valuesKeyword, values) =>
      val anonTableName = qc anonymizeName tableName
      val anonFieldNameArray = fieldNames split ", " map qc.anonymizeName // encrypt field names
      try {
        val fieldTypes = anonFieldNameArray map (sc.getFieldDatatype(anonDatabaseName, anonTableName, _)) // get their datatypes
        val anonValues = (qc encryptValueArray (
          fieldTypes zip (values split ", ")) flatten ) mkString ", "   // encrypt values
        val anonFieldNames = (anonFieldNameArray zip fieldTypes) map (x =>
          qc.translateFieldName(x._1, x._2) mkString ", "
          ) mkString ", "
        //println(s"$insertKeyword`$anonTableName` ($anonFieldNames)$valuesKeyword($anonValues)")
        // issue the INSERT statement
        client.executeUpdate(s"$insertKeyword`$anonTableName` ($anonFieldNames)$valuesKeyword($anonValues)")
      }
      catch {
        case e: SQLException => throw e
      }
 

    case updateSetRegex(updateKeyword, tableName, setKeyword, assignments, whereKeyword, conditions) =>
      val anonTableName = qc anonymizeName tableName
      val assignmentArray = assignments split ", "
      /* Store tuples for assignments with format "fieldName = value"
       * Stores the field name, value, and respective datatype */
      var directAssignments: Array[(String, String, String)] = Array.empty
      /* Store tuples for assignments with format "fieldName = fieldName + value"
       * Stores the field name, the operator and the value. Datatype is implicitly INT */
      var incrementAssignments: Array[(String, String, String)] = Array.empty
      // Analyze and collect assigments to the appropriate array
      try {
        for (a <- assignmentArray) {
          a match {
            case updateAssignmentRegex(fieldName, value1, operator, value2) =>
              if (operator != null && value2 != null && fieldName == value1)
                // increment assignments are implicitly related to INT, so no datatype is required
                // store tuple with (fieldName, increment value)
                incrementAssignments :+= (qc anonymizeName fieldName, operator, value2)
              else {
                val anonFieldName = qc anonymizeName fieldName
                // store a tuple with (fieldName, value, datatype)
                directAssignments :+= (anonFieldName, value1, sc.getFieldDatatype(anonDatabaseName, anonTableName, anonFieldName))
              }
          }
        }
        // anonymize Where conditions
	var anonConditions = ""
	if (whereKeyword != null)
          anonConditions = translateWhereConditions(null, conditions)

        var anonAssignments = ""
        if (incrementAssignments.length > 0) {
          // obtain prefixed field names (only OPE for now)
          val staleFieldNames = incrementAssignments map (a => qc.translateOpe(a._1))
          // issue a SELECT of the values that will become stale after ADD operation (same WHERE conditions)
          val valueResultSet = selectStaleValues(anonTableName, staleFieldNames, anonConditions)
          var setCaseStrings: Array[String] = new Array[String](staleFieldNames.length)
          for (i <- 0 to staleFieldNames.length - 1) setCaseStrings(i) = s"${staleFieldNames(i)} = CASE"
          // traverse Result Set and build SET CASE statements
					while (valueResultSet.next) {
         	  for (i <- 0 to staleFieldNames.length - 1) {
         	    val oldValueEnc = valueResultSet.getLong(i + 1)
         	    val newValue = qc.decryptOpe(oldValueEnc) + (incrementAssignments(i)._3.toInt)
         	    val newValueEnc = qc.encryptOpe(newValue)
         	    setCaseStrings(i) += s" WHEN ${staleFieldNames(i)} = ${oldValueEnc.toString} THEN ${newValueEnc.toString}"
         	  }
         	}
         	for (i <- 0 to staleFieldNames.length - 1)
         	  setCaseStrings(i) += s" ELSE ${staleFieldNames(i)} END"
         	// add SET CASE strings and Homomorphic Add UDF calls to anonAssignments String
         	anonAssignments += setCaseStrings mkString ", "
          for (a <- incrementAssignments) {
            anonAssignments += s", ${qc.translateSetAddUdfCall(a._1, a._3)}"
          }
          if (directAssignments.length > 0)
            anonAssignments += ", "
        }
        
        anonAssignments += directAssignments map { a =>
          val assignmentPairs = (qc.translateFieldName(a._1, a._3)) zip (qc.encryptValue(a._2, a._3))
          assignmentPairs map { p =>
            s"${p._1} = ${p._2}"
          } mkString ", "
        } mkString ", "
        // Issue the UPDATE statement
        //println(s"$updateKeyword `$anonTableName` $setKeyword $anonAssignments ${if (whereKeyword != null) whereKeyword else ""}$anonConditions")
        client.executeUpdate(s"$updateKeyword `$anonTableName` $setKeyword $anonAssignments ${if (whereKeyword != null) whereKeyword else ""} $anonConditions")
      }
      catch {
        case e: SQLException => throw e
      }


      case deleteRegex(deleteKeyword, tableName, whereKeyword, conditions) =>
        val anonTableName = qc anonymizeName tableName
        if (whereKeyword != null) {
          val anonConditions = translateWhereConditions(null, conditions)
          //println(s"$deleteKeyword`$anonTableName`$whereKeyword$anonConditions")
          client.executeUpdate(s"$deleteKeyword`$anonTableName`$whereKeyword$anonConditions")
        }
        else {
          //println(s"$deleteKeyword`$anonTableName`")
          client.executeUpdate(s"$deleteKeyword`$anonTableName`")
        }

    case _ => 0

  }


/* ----- Auxiliary methods for executeQuery --------------------------------- */


  /*
   * Returns a tuple with the anonymized table and field names, but the field name is
   * not prefixed with any encryption scheme identifier.
   * */
  def anonymizeFullFieldNameNoScheme (anonNamesMap: Map[String, String], fieldName: String): (String, String) = {
    val splitName = fieldName split '.'
    (anonNamesMap(splitName(0)), qc anonymizeName splitName(1))
  }


  /*
   * Anonymize the given fieldName with format "tableName.fieldName"
   * for the default encryption scheme of its datatype.
   * */
  @throws(classOf[SQLException])
  def anonymizeFullFieldNameDefaultScheme (anonNamesMap: Map[String, String],
      fieldName: String): (String, QueryCryptoUtils.Scheme, String) = {
    val splitName = fieldName split '.'
    val anonTableName = anonNamesMap(splitName(0))
    val anonFieldName = qc anonymizeName splitName(1)
    try {
      val fieldDatatype = sc.getFieldDatatype(anonDatabaseName, anonTableName, anonFieldName)
      val anonFieldNameTuple = qc.translateDefaultTableFieldName(anonTableName, anonFieldName, fieldDatatype)
      (s"${anonFieldNameTuple._1}", anonFieldNameTuple._2, fieldDatatype)
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Anonymize the given fieldname for the default encryption scheme of its datatype.
   * */
  @throws(classOf[SQLException])
  def anonymizeFieldNameDefaultScheme (anonTableName: String,
      fieldName: String): (String, QueryCryptoUtils.Scheme, String) = {
    val anonFieldName = qc anonymizeName fieldName
    try {
      val fieldDatatype = sc.getFieldDatatype(anonDatabaseName, anonTableName, anonFieldName)
      val anonFieldNameTuple = qc.translateDefaultFieldName(anonFieldName, fieldDatatype)
      (s"${anonFieldNameTuple._1}", anonFieldNameTuple._2, fieldDatatype)  
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Translate a selection of a field with format "tableName.fieldName".
   * Matches and translates a function over a field, or simply a field.
   * Simple selections are translated to use a default scheme, but some
   * aggregate functions might require a specific scheme.
   * 
   * Example: a select of an INT field is translated to a select of the OPE
   * column, but a SUM function over the same field requires ADD.
   * */
  def translateFullFieldSelection (anonNamesMap: Map[String, String],
      selection: String): (String, QueryCryptoUtils.Scheme, String) = {
    selection match {
      
      // count result is not encrypted
	  // TODO: Implement encryption for count
      case countSelectionRegex (countKeyword, fieldName) =>
        if (fieldName equals "*")
          (selection, QueryCryptoUtils.NoScheme, QueryCryptoUtils.IntegerType)
        else {
          val anonFieldNameTuple = anonymizeFullFieldNameDefaultScheme(anonNamesMap, fieldName)
          (s"$countKeyword(${anonFieldNameTuple._1})", QueryCryptoUtils.NoScheme, QueryCryptoUtils.IntegerType)
        }
        
      // COALESCE is always performed on OPE in our test cases
      // only predicted case: COALESCE(fieldSelection,value)
      // IMPORTANT: no space after comma
      case coalesceSelectionRegex (coalesceKeyword, values) =>
        val coalesceParams = values split "," filter (_ != "")
        val anonFieldParam = translateFullFieldSelection(anonNamesMap, coalesceParams(0))._1
        val anonValue = qc.encryptOpe(coalesceParams(1).toInt)
        val anonCoalesce = s"$coalesceKeyword($anonFieldParam, $anonValue)"
        (anonCoalesce, QueryCryptoUtils.Ope, QueryCryptoUtils.IntegerType)

      // MAX/MIN is always performed on OPE
      case maxMinSelectionRegex (maxMinKeyword, fieldName) =>
        val anonFieldNameTuple = anonymizeFullFieldNameDefaultScheme(anonNamesMap, fieldName)
        (s"$maxMinKeyword(${anonFieldNameTuple._1})", anonFieldNameTuple._2, anonFieldNameTuple._3)

      // SUM result is ADD
      case sumSelectionRegex (sumKeyword, fieldName) =>
        val anonFieldNamePair = anonymizeFullFieldNameNoScheme(anonNamesMap, fieldName)
        val aggSumCall = qc.translateAggAddUdfCall(s"`${anonFieldNamePair._1}`.${qc.translateAdd(anonFieldNamePair._2)}")
        (aggSumCall, QueryCryptoUtils.Add, QueryCryptoUtils.IntegerType)

	  // Image field selection
	  case imageSelectionRegex (imageKeyword, fieldName) =>
		val anonFieldNameTuple = anonymizeFullFieldNameDefaultScheme(anonNamesMap, fieldName)
		// image identifiers are plaintext integers
		(anonFieldNameTuple._1, QueryCryptoUtils.NoScheme, QueryCryptoUtils.IntegerType)

      // simple field select, provide default
      case _ =>
        anonymizeFullFieldNameDefaultScheme(anonNamesMap, selection)

    }
  }


  /*
   *
   * */
  def translateFieldSelection (anonTableName: String,
      selection: String): (String, QueryCryptoUtils.Scheme, String) = {
    selection match {

      // count result is not encrypted
	  // TODO: Implement encryption for count
      case countSelectionRegex (countKeyword, fieldName) =>
        if (fieldName equals "*")
          (selection, QueryCryptoUtils.NoScheme, QueryCryptoUtils.IntegerType)
        else {
          val anonFieldNameTuple = anonymizeFieldNameDefaultScheme(anonTableName, fieldName)
          (s"$countKeyword(${anonFieldNameTuple._1})", QueryCryptoUtils.NoScheme, QueryCryptoUtils.IntegerType)
        }
        
      // COALESCE is always performed on OPE in our test cases
      // only predicted case: COALESCE(fieldSelection,value)
      // IMPORTANT: no space after comma
      case coalesceSelectionRegex (coalesceKeyword, values) =>
        val coalesceParams = values split "," filter (_ != "")
        val anonFieldParam = translateFieldSelection(anonTableName, coalesceParams(0))._1
        val anonValue = qc.encryptOpe(coalesceParams(1).toInt)
        val anonCoalesce = s"$coalesceKeyword($anonFieldParam, $anonValue)"
        (anonCoalesce, QueryCryptoUtils.Ope, QueryCryptoUtils.IntegerType)

      // MAX/MIN is always performed on OPE
      case maxMinSelectionRegex (maxMinKeyword, fieldName) =>
        val anonFieldNameTuple = anonymizeFieldNameDefaultScheme(anonTableName, fieldName)
        (s"$maxMinKeyword(${anonFieldNameTuple._1})", anonFieldNameTuple._2, anonFieldNameTuple._3)

      // SUM result is ADD
      case sumSelectionRegex (sumKeyword, fieldName) =>
        val anonFieldName = qc anonymizeName(fieldName)
        val aggSumCall = qc.translateAggAddUdfCall(s"${qc.translateAdd(anonFieldName)}")
        (aggSumCall, QueryCryptoUtils.Add, QueryCryptoUtils.IntegerType)

	  case imageSelectionRegex (imageKeyword, fieldName) =>
		val anonFieldName = qc anonymizeName(fieldName)
		// image identifiers are plaintext integers
		(anonFieldName, QueryCryptoUtils.NoScheme, QueryCryptoUtils.IntegerType)

      // simple field select, provide default
      case _ =>
        anonymizeFieldNameDefaultScheme(anonTableName, selection)

    }
  }


  /*
   *
   * */
  def processJoinFields (joinConditions: String, anonNamesMap: Map[String, String]): Array[String] = {
    val joinFieldsArray = joinConditions.split(" = ").sliding(2,2).toArray
    for (c <- joinFieldsArray) yield {
      // c(0) = field1, c(1) = field2
      anonymizeFullFieldNameDefaultScheme(anonNamesMap, c(0))._1 +
        " = " + anonymizeFullFieldNameDefaultScheme(anonNamesMap, c(1))._1
    }
  }


  /*
   * Processes and anonymizes a query.
   * */
  @throws(classOf[SQLException])
  def translateExecuteQuery (query: String): (String, Int, Array[QueryCryptoUtils.Scheme], Array[String]) = {
    
    try {

      query match {
  
        case selectRegex(selectKeyword, forUpdateKeyword, fieldSelections, fromKeyword, tableNames, whereKeyword, conditions, orderBy, orderSelection) =>

          //println(s"select: $selectKeyword\nfieldSelections: $fieldSelections\nfrom: $fromKeyword\ntableNames: $tableNames\nwhere $whereKeyword\nconditions: $conditions\norderby: $orderBy\norderSelection: $orderSelection")

          var anonNamesMap: Map[String, String] = Map()
          var anonTableName = ""
          var anonConditions = ""
          var anonOrderSelection = ""
          var hasJoin: Boolean = false
		  
          /* Match either a Join of tables, or a single table name */
          val anonTableSelection = tableNames match {
            case tableJoinRegex(table1, joinKeyword, table2, onKeyword, joinConditions) =>
              val anonTable1 = qc anonymizeName table1
              val anonTable2 = qc anonymizeName table2
              anonNamesMap += (table1 -> anonTable1, table2 -> anonTable2)
              hasJoin = true
              s"`$anonTable1` JOIN `$anonTable2` ON " + (processJoinFields(joinConditions, anonNamesMap) mkString " AND ")

            case _ =>
              anonTableName = qc anonymizeName tableNames
              s"`$anonTableName`"
          }

          if (whereKeyword != null)
            anonConditions = s"$whereKeyword" + translateWhereConditions(anonNamesMap, conditions)

          val (anonFieldSelections, schemes, datatypes) = if (hasJoin)
            (fieldSelections split ", " filter (_ != "") map ( s =>
              translateFullFieldSelection(anonNamesMap, s)
            ) unzip3)
          else
            (fieldSelections split ", " filter (_ != "") map ( s =>
              translateFieldSelection(anonTableName, s)
            ) unzip3)

          if (orderBy != null) {
            if (hasJoin)
              anonOrderSelection = s"$orderBy${translateFullFieldSelection(anonNamesMap, orderSelection)._1}"
            else
              anonOrderSelection = s"$orderBy${translateFieldSelection(anonTableName, orderSelection)._1}"
          }

          /* Issue the executeQuery but don't decrypt the ResultSet here. Decrypt it later when it is streamed to client
           * to avoid traversing it twice. */
          val query = s"$selectKeyword ${anonFieldSelections mkString ", "}$fromKeyword$anonTableSelection$anonConditions$anonOrderSelection" + (if (forUpdateKeyword != null) forUpdateKeyword else "")
          //println(query)
		  (query, anonFieldSelections.size, schemes, datatypes)

        /*
        case _ =>
          None
        */    
      }
    }
    catch {
      case e: SQLException => throw e
    }

  }


  def executeQuery (query: String): ResultSetRowIterator = {
    /*
     if (rs != null)
     rs.cancel
     * */

    /* 1: query string
     * 2: number of columns
     * 3: schemes
     * 4: datatypes */
    val anonymizedQueryData = translateExecuteQuery(query)
    rs = client.executeQuery(anonymizedQueryData._1, anonymizedQueryData._2)
    new ResultSetRowIterator(rs, qc, (anonymizedQueryData._3, anonymizedQueryData._4))
  }


  /* ------------------------------------------------------------------------ */
  /* --------- CBIR Operations ---------------------------------------------- */


  /*
   * Just issue the index operation
   * */
  def indexImages: MIEResponse = {
    client.indexImages
  }


  /*
   * Encrypt image and features, issue addImage operation
   * */
  def addImage (id: Int, image: Array[Byte]): MIEResponse = {
    val bytes = ic.encryptImage(id, image)
    client.addImage(-1, bytes) // image id already contained in bytes
  }


  /*
   * Encrypt image features (not the image), issue search operation
   * */
  def searchImage (image: Array[Byte]): MIESearchResultArray = {
    val bytes = ic getImageFeatures image
    client.searchImage(bytes)
  }


  /*
   * Issue an image retrieval operation, decrypt obtained image
   * */
  def getImage (id: Int): MIEImage = {
    val mieEncryptedImage = client.getImage(id)
    val imageBytes = ic.decryptImage(mieEncryptedImage.bytes, mieEncryptedImage.unzippedDataSize)
    MIEImage(imageBytes, 0)
  }


  /* ------------------------------------------------------------------------ */
  /* --------- Multimodal SQL + CBIR Operations ----------------------------- */


  /*
   *
   * */
  def multimodalQuery (sqlQuery: String, conjunction: String, imageField: String, imageBytes: Array[Byte]): ResultSetRowIterator = {
    /*
      if (rs != null)
      rs.cancel
		*/

    val anonymizedQueryData = translateExecuteQuery(sqlQuery)
    val anonImageFieldName = qc translateImg (qc anonymizeName imageField)
    val encryptedImageBytes = ic.getImageFeatures(imageBytes)

    rs = client.executeMultimodalQuery(anonymizedQueryData._1, conjunction, anonImageFieldName, encryptedImageBytes, anonymizedQueryData._2)
    new ResultSetRowIterator(rs, qc, (anonymizedQueryData._3, anonymizedQueryData._4))
  }


}
