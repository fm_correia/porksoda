package queryProcessing

import scala.collection.mutable.HashMap


@SerialVersionUID(100L)
class DatabaseSchema extends Serializable {

  private var tableMap: HashMap[String, TableSchema] = HashMap.empty

  def getTableSchema (tableName: String): TableSchema = tableMap(tableName)

  def insertTableSchema (tableName: String, tableSchema: TableSchema) = tableMap += (tableName -> tableSchema)

  def removeTableSchema (tableName: String) = tableMap -= tableName

}


@SerialVersionUID(101L)
class TableSchema extends Serializable {

  private var fieldMap: HashMap[String, FieldInfo] = HashMap.empty

  def getField (fieldName: String): FieldInfo = fieldMap(fieldName)

  def insertField (fieldName: String, fieldInfo: FieldInfo) = fieldMap += (fieldName -> fieldInfo)

  def removeField (fieldName: String) = fieldMap -= fieldName

}


@SerialVersionUID(102L)
class FieldInfo (plaintextType: String) extends Serializable {

  //import java.util.concurrent.locks.{StampedLock}
  
  private val pType: String = plaintextType 

  /*
  private var layer: Int = 0
  var lock: StampedLock = new StampedLock

  def getLayer: Int = layer
  def setLayer (level: Int) = { layer = level }
  */

  def getFieldDatatype (): String = pType

}


object Schema {

  import java.io.{ObjectInputStream, ObjectOutputStream, FileInputStream, FileOutputStream}

  var databaseSchemaMap: HashMap[String, DatabaseSchema] = HashMap.empty

  @throws(classOf[Throwable])
  def loadSchema (path: String) = {
    try {
      val ois = new ObjectInputStream(new FileInputStream(path))
      databaseSchemaMap = ois.readObject.asInstanceOf[HashMap[String, DatabaseSchema]]
      ois.close
    }
    catch {
      case e: Throwable => throw e
    }
  }

  @throws(classOf[Throwable])
  def saveSchema (path: String) = {
    try {
      val oos = new ObjectOutputStream(new FileOutputStream(path))
      oos.writeObject(databaseSchemaMap)
      oos.close
    }
    catch {
      case e: Throwable => throw e
    }
  }

}



class Schema {

  import Schema._


  /*
   * ATTENTION: These methods are NOT THREAD SAFE!
   * */


  def checkSchemaExists (databaseName: String): Boolean = {
    databaseSchemaMap contains databaseName
  }

  /*
   * Checks if a schema with given databaseName exists.
   * If not, adds a new schema to databaseSchemaMap.
   * Returns true if schema was added, false otherwise.
   * */
  def createDatabaseSchema (databaseName: String): Boolean = {
    if (databaseSchemaMap contains databaseName) {
      false
    }
    else {
      databaseSchemaMap += (databaseName -> new DatabaseSchema)
      true
    }
  }

  def deleteDatabaseSchema (databaseName: String) = {
    databaseSchemaMap -= databaseName
  }

  def getDatabaseSchema (databaseName: String): DatabaseSchema = {
    databaseSchemaMap(databaseName)
  }

  def createTableSchema (databaseName: String, tableName: String) = {
    var databaseSchema = getDatabaseSchema(databaseName)
    databaseSchema.insertTableSchema(tableName, new TableSchema)
  }

  def deleteTableSchema (databaseName: String, tableName: String) = {
    var databaseSchema = getDatabaseSchema(databaseName)
    databaseSchema removeTableSchema tableName
  }

  def getTableSchema (databaseName: String, tableName: String): TableSchema = {
    getDatabaseSchema(databaseName).getTableSchema(tableName)
  }

  def insertFields (databaseName: String, tableName: String, fieldPairs: Array[(String, String)]) = {
    var tableSchema = getTableSchema(databaseName, tableName)
    fieldPairs foreach (p => tableSchema.insertField(p._1, new FieldInfo(p._2)))
  }

  def getField (databaseName: String, tableName: String, fieldName: String): FieldInfo = {
    getTableSchema(databaseName, tableName).getField(fieldName)
  }

  
  def getFieldDatatype (databaseName: String, tableName: String, fieldName: String): String = {
    getField(databaseName, tableName, fieldName).getFieldDatatype
  }

  /*
  def getFieldLayer (databaseName: String, tableName: String, fieldName: String): Int = {
    getField(databaseName, tableName, fieldName).getLayer
  }

  def setFieldLayer (databaseName: String, tableName: String, fieldName: String, layer: Int) = {
    getField(databaseName, tableName, fieldName).setLayer(layer)
  }

  def acquireFieldReadLock (databaseName: String, tableName: String, fieldName: String): Long = {
    getField(databaseName, tableName, fieldName).lock.readLock
  }

  def acquireFieldWriteLock (databaseName: String, tableName: String, fieldName: String): Long = {
    getField(databaseName, tableName, fieldName).lock.writeLock
  }

  def releaseFieldReadLock (databaseName: String, tableName: String, fieldName: String, lockStamp: Long) = {
    getField(databaseName, tableName, fieldName).lock.unlockRead(lockStamp)
  }

  def releaseFieldWriteLock (databaseName: String, tableName: String, fieldName: String, lockStamp: Long) = {
    getField(databaseName, tableName, fieldName).lock.unlockWrite(lockStamp)
  }
  */

}
