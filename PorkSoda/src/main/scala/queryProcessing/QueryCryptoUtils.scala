package queryProcessing

import hlib.hj.mlib._
import scala.collection.mutable.HashMap
import java.nio.charset.Charset
import java.math.BigInteger
import java.sql.ResultSet


object QueryCryptoUtils {

  /*
   * !!! HORRENDOUS MALPRACTICE WARNING !!!
   * Hardcoded encryption keys and parameters!
   * */

  // Passphrases for key generation
  private final val OpePassphrase = "Jerry was a racecar driver"
  private final val DetPassphrase = "rO0ABXNyAB9qYXZheC5jcnlwdG8uc3BlYy5TZWNyZXRLZXlTcGVjW0cLZuIwYU0CAAJMAAlhbGdvcml0aG10ABJMamF2YS9sYW5nL1N0cmluZztbAANrZXl0AAJbQnhwdAADQUVTdXIAAltCrPMX+AYIVOACAAB4cAAAABBt9ii6RMjQB7seqMbJonI9"
  private final val PaillierKeyString = "rO0ABXNyABhobGliLmhqLm1saWIuUGFpbGxpZXJLZXkAAAAAAAAAAQIAB0wAAWd0ABZMamF2YS9tYXRoL0JpZ0ludGVnZXI7TAAGbGFtYmRhcQB+AAFMAAJtdXEAfgABTAABbnEAfgABTAAHbnNxdWFyZXEAfgABTAABcHEAfgABTAABcXEAfgABeHBzcgAUamF2YS5tYXRoLkJpZ0ludGVnZXKM/J8fqTv7HQMABkkACGJpdENvdW50SQAJYml0TGVuZ3RoSQATZmlyc3ROb256ZXJvQnl0ZU51bUkADGxvd2VzdFNldEJpdEkABnNpZ251bVsACW1hZ25pdHVkZXQAAltCeHIAEGphdmEubGFuZy5OdW1iZXKGrJUdC5TgiwIAAHhw///////////////+/////gAAAAF1cgACW0Ks8xf4BghU4AIAAHhwAAACADGIVGMN1/ZTLfmps59sBgMDzYW5sDx73RIvs6frRJI3xoD0Jsdh3RCtbUfG8mJtNhzC9THdcAwUmAycgXEsNWht8ktHQe2Xyw2aauu2D/s6LzLu/Pqz9AnMeh1Num8bI2fdFID3l1zkQSRMSXS9jkpuzPJIig2PVeVUvuhrUiK0+Fy7BheZLPDk7r/WjeDLCFKHEfII9c1X/1gFUT+LI/Xqg/zCAyOT1LOJ+ni1/T1JFMVCnl/c9VeaIFW1ghLh5gpJ1KTTFW4mEjJkPXLgorY1iPnicIoudxypHjKXQCw1qoOMLNGBKlWFY5ttoSHV2/a31cOQRfXxSmysbnM8YnQe+P5B+/keLzj4CB7y/RDk4HSqJgbOuRzTCAgjH7qXzz90XEiEtu12eNmnwWd8jaU+fUptkSgwKMV6vNg5CQ6w6/sK9FOHGJU4QOI/2eSjLp6hzEHhOCEBFuR2D3Nzy5+CBW3IgpYw7I2FpgNzhv8aVEk6xhosxI4c5cdME+twSmvGo0vg+kv5MoMVfTXOj2v3BtrEVLPqKRWFh4RyCacIjY+e7nAaqzUFhGyZcq02eeI8iDIdDopmoKqdtX1dvNPWOQlg6Ny3oYjEDMdSUG3ftNHPUPXkajBCJDGNNwpP2Lii64Azrsj8YYPCNVEkWenR9LQB0PMJy+NAf2A1tRsgeHNxAH4AA////////////////v////4AAAABdXEAfgAHAAAA/7dZAM2ns8BPAJ82p28nzZ2ynx1KrKc5fubiMyeip70LRmwloARomLFCxSkce9AIkYlzg5qAvw3JvO4nf24dY8YiCkvdH6bxkVsgcjI5tBz1Jscb/BG2I3Yox3qE+af0NmVVQ9Gu2fLZglk3LvK5i0hjPq9FCcS/derr4gT9r8vJd7p2WQ7l2O0vMvrCoSBb0byOQ3c5cu1cEB7Q9U/aFw9Q7Pd5gYsZ8PzLHKUV6NeTMXvptiaqij7jwb4ONZpxU06opUIi5+m4E8ID8cMSwN1yeH7yiniOLqts0QK2TRRf4au7sHl0HIR/Ipuyy/HGQfvcUQC96wyXPUKlbR2JTHhzcQB+AAP///////////////7////+AAAAAXVxAH4ABwAAAQAVV9DhKQyxtt1xdX5xHQ7Iog/impyK/vl+kCCxc6Rfujvk6ELkUi6QZ4rIFsNQ3AesKiwrd2Lohhox2BDNtwDqMrUfCEcF4OsgWji3fOWbiv4dCkbsqmwNgvcqmdXA9jIClqhhmbgT3Zlm3UU47RJ1NsDQcJHJGtNskK4MeTNBr5+fYfF20g/hiQSGfn9v4wOx8uk0oI85IC+Jtm8QkmS1mvn7ItbuqyqvtR7VWLrNw9M78NGDo1euZ49wE+4Ie2T+/pQ7mFfEX2IrLoo5/JGPF9EbUswOW31edvicecc5ep2lrSUCnWFd32PoHhJYVRDwTXA9CZ1b7K3gZWtyPm+3eHNxAH4AA////////////////v////4AAAABdXEAfgAHAAABAIDqlJCZ6mM3jG/ybbon/JLhl+CYgWWUbTpXC/feXfDr7YQKdIMZi1yi8qDoBw5GBlSlOIiihlWx2NdzxZlsqidP7z1XekFh2jQS0EtQkqRcX0P/rTx0EO8UrEIlf4oXtj8/869u8T7A76a6zQKqde7lyBM8it5Wnukt2uuCX5tLFfuZQE1M7mbrcyxQOn3MgRdW7T95kT9e7a+pbMVJbo0BCjf0mp3MpEKpIRLKNPYozxMFbKye71cShk//POzCsGHuRrMckX2UN72C4sqxauLvqD2UzXPrLgV3ICfBGXonxSjA1J0yV9vehFao74I8vrKRBqnU1sp+mOfMOCDi3jF4c3EAfgAD///////////////+/////gAAAAF1cQB+AAcAAAIAQOtrhIh7R1l07okmRt53dtrkDJLbygbuNWoQYUihUBtDBDg4NhstiXU/fl7S4+tvc309RSm+fhevrygu3ovRQ1P+nqiUfl+jI0enZGPAyo0J9wg3TjdU0ELFkGQVj7kLef9Xl0xbJn1W7+Ck9DsgxGnqatpZg+1kadCfq+jb8JmNB9DO3IySziMRXoCh6OoGmASLqGLnS4oGhPEmMLf2doOaPJvRZg3rEmLsJlOkeeoWARUWQBT5ItuTOe2ebKE3CEsfAZyGSuc0a+im7o95tCDVCEv2WIIpgjg6wIKbZNpAdBd7Q4k4Uf5VbKFd9mrfhiN+ZVGJi5MbPNO6JkEJ8VKv+wXLDfV8BRZfxBTkfYvDl0/eR7j4C0/mzzo6ET3Bg+DMUO+y/6Ceah96xaN2IZzgoUnPQQTWxwW37NF+DKKD6R/b8YYjN/HFYE8xfk4dsr3YHcE9yrfj05a1yQHI58g9au3RX/tO77InLTmxc3vgME0Plq/ifcpOzb9/lGd5gPvWEyRfmxe3Jtp6kGHvCDu50fMJPjFIhpPgy32m/Qxn7W/16zQ4Pztv8DHzQfzoGL1xyiyKvhhqOs889EHZBrQ5Yv/IK59yrdvECGQBjdDn9VQ8M8YZ4TKtXw5Bm8e+2pFuWR0M0s3B4EzRTq9gDPeOE4nfJk47JgbOCU9dBWF4c3EAfgAD///////////////+/////gAAAAF1cQB+AAcAAACAobIFTXt3Le5kK11od8bKn1FZyMcnXeHDr3Fk5n+ZQGxiqxng2ypIqU1yZAq43k6eDUseDih2V3z+o2QPbBn/bzXTahocdBqeUu3iUsMbbJdMBx6912uCjLNl9RF5zTqJ9iXDs14+M/GootiH0ClMh+tBNKKvF/BnVxW/p8iowC14c3EAfgAD///////////////+/////gAAAAF1cQB+AAcAAACAzBp4vDNcI/HCF/aW6WxBUk1pHQh9zo62guiZmcmK1enZeIASUF21vYN37OP97u/3QvzHCnD4rqXbvrJN0yIrkpjMSGZ/klKgdus1ygpszq/vJGWUbKukqIGIqCBfHg1KZFY9KSmOf90MfSinZ/Lw0GI46RKgNZU86r8wP591lJV4"

  /*
   * SecretKey objects for each encryption scheme
   * OPE scheme needs to instantiate a HomoOpeInt class object. Created in the class, not here
   * */
  private final val DetSecretKey = HomoDet keyFromString DetPassphrase
  private final val AddSecretKey = HelpSerial.fromString(PaillierKeyString).asInstanceOf[PaillierKey]

	// enable Paillier cache
	private final val Cache = true 

  
  // Homomorphic encryption scheme identifiers within this class.
  sealed trait Scheme extends Product with Serializable
  final case object Rnd extends Scheme
  final case object Det extends Scheme
  final case object Ope extends Scheme
  final case object Add extends Scheme
  final case object NoScheme extends Scheme

  // Prefixes for anonymized field names
  final val DetPrefix = "DET_"
  final val OpePrefix = "OPE_"
  final val AddPrefix = "ADD_"
  final val ImagePrefix = "IMG_"

  // Supported SQL types
  final val IntegerType = "INT"
  final val VarcharType = "VARCHAR"
  final val CharType = "CHAR"
  final val TextType = "TEXT"
  final val ImageType = "IMAGE"	// not an SQL type, but image reference fields will be declared as such

  // Size (in bytes) of SQL datatype units.
  private final val IntegerSize = 4
  private final val VarcharSize = 1
  private final val CharSize = 1

  // SQL datatypes for encryption scheme support.
  private final val DetDatatype = "VARCHAR"
  private final val DetTextDatatype = "TEXT"
  private final val OpeDatatype = "BIGINT"
  private final val AddDatatype = "VARCHAR" // instead of CHAR because of length restrictions
  private final val ImgDatatype = "INT"

  /*
   * Block size (in bytes) of encryption schemes.
   * Does not apply to OPE.
   * */
  private final val DetBlockSize = 16
  private final val AddBlockSize = 1233


/*
  // Mapping of plaintext SQL datatypes to supported encryption scheme identifiers.
  final val plaintextDatatypeSchemes: Map[String, Array[Scheme]] = Map(
    IntegerType -> Array(Ope, Add),
    VarcharType -> Array(Det),
    CharType    -> Array(Det)
  )

  // Mapping of encryption schemes to SQL datatype representations.
  final val schemeDatatypes: Map[Scheme, String] = Map(
    Det -> DetDatatype,
    Ope -> OpeDatatype,
    Add -> AddDatatype
  )

  // Mapping of encryption schemes to ciphertext block sizes in bytes (for those applicable).
  final val schemeBlockSizes: Map[Scheme, Int] = Map(
    Det -> DetBlockSize,
    Add -> AddBlockSize
  )
*/
  
  // User Defined Function names
  final val SumUDF = "crypto_sum"
  final val AggSumUDF = "crypto_agg_sum"

  // for decoding of byte arrays
  private final val UTF8 = Charset.forName("UTF-8")

}


class QueryCryptoUtils {


  import QueryCryptoUtils._


  // HomoOpeInt class object must be instantiated here
  var opeOperations = new HomoOpeInt(OpePassphrase)
 
  val detCipherCache: HashMap[String, String] = if (Cache) HashMap.empty else null
  val opeCipherCache: HashMap[Int, Long] = if (Cache) HashMap.empty else null
  val paillierCipherCache: HashMap[String, String] = if (Cache) HashMap.empty else null


  /*
   *
   * */
  def decryptOpe (value: Long): Int = {
    if (value == 0)
      0
    else
      opeOperations.decrypt(value)
  }


  /*
   *
   * */
  def decryptAdd (value: String): Int = {
    if (value == null)
      0
    else
      HomoAdd.decrypt(new BigInteger(value), AddSecretKey).intValue
  }


  /*
   *
   * */
  def decryptTextDet (value: String): String = {
    if (value == null)
      null
    else
      HomoDet.decrypt(DetSecretKey, value)
  }


  /*
   *
   * */
  def decryptValue (value: Object, valueInfo: (Scheme, String)): Object = {
    valueInfo match {
      case (Det, VarcharType) =>
        decryptTextDet(value.asInstanceOf[String]).asInstanceOf[Object]
      case (Det, CharType) =>
        decryptTextDet(value.asInstanceOf[String]).asInstanceOf[Object]
      case (Det, TextType) =>
        decryptTextDet(value.asInstanceOf[String]).asInstanceOf[Object]
      case (Ope, IntegerType) =>
        decryptOpe(value.asInstanceOf[Long]).asInstanceOf[Object]
      case (Add, IntegerType) =>
        // getObject returns Array[Byte] when String is too big
        decryptAdd(new String(value.asInstanceOf[Array[Byte]], UTF8)).asInstanceOf[Object]
      case (NoScheme, IntegerType) =>
        value.asInstanceOf[Long].toInt.asInstanceOf[Object]
	  case (NoScheme, ImageType) =>
		value
    }
  }


  /*
   * Return OPE encryption of the provided Integer value.
   * */
  def encryptOpe (value: Int): Long = {
    if (Cache) {
      (opeCipherCache get value) match {
        case Some(encVal) => return encVal
        case None => {
          val encVal = opeOperations.encrypt(value)
          opeCipherCache += (value -> encVal)
          return encVal
        }
      }
    }
    else
      opeOperations.encrypt(value)
  }


  /*
   * Return ADD encryption of the provided Integer value.
   * */
  def encryptAdd (value: String): String = {
    if (Cache) {
      (paillierCipherCache get value) match {
        case Some(encVal) => return encVal
        case None => {
          val encVal = (HomoAdd.encrypt(new BigInteger(value), AddSecretKey) toString)
          paillierCipherCache += (value -> encVal)
          return encVal
        }
      }
    }
    else
      HomoAdd.encrypt(new BigInteger(value), AddSecretKey) toString
	}


  /*
   * Return DET encryption of provided text value.
   * */
  def encryptTextDet (value: String): String = {
    if (Cache) {
      (detCipherCache get value) match {
        case Some(encVal) => return encVal
        case None => {
          val encVal = HomoDet.encrypt(DetSecretKey, value)
          detCipherCache += (value -> encVal)
          return encVal
        }
      }
    }
    else
      HomoDet.encrypt(DetSecretKey, value)
  }


  /*
   * Returns a tuple of all supported encryptions of the provided Integer value.
   * */
  def encryptIntegerValue (value: String): (Long, String) = {
    (encryptOpe(value.toInt), encryptAdd(value))
  }


  /*
   * Returns all supported encryptions of the provided Char/Varchar value.
   * */
  def encryptCharValue (value: String): String = {
    encryptTextDet(value)
  }


  /*
   * Returns an Array of all supported encryptions for its datatype.
   * */
  def encryptValue (value: String, datatype: String): Array[String] = {
    datatype match {
      case IntegerType =>
        val encVal = encryptIntegerValue(value)
        Array(encVal._1.toString, s"'${encVal._2}'")
      case VarcharType =>
        Array(s"'${encryptTextDet(value)}'")
      case CharType =>
        Array(s"'${encryptTextDet(value)}'")
      case TextType =>
        Array(s"'${encryptTextDet(value)}'")
	  case ImageType =>
		Array(value)	// no encryption
    }
  }


  /*
   * Given an Array of pairs (datatype, value), encrypts all values accordingly
   * and returns an Array of Arrays of the encryptions.
   * */
  def encryptValueArray (values: Array[(String, String)]): Array[Array[String]] = {
    values map { v =>
      v._1 match {
        case IntegerType =>
        if (v._2.toLowerCase == "null")
          Array("null", "null")
        else {
          val intEncryptedTuple = encryptIntegerValue(v._2)
          Array(intEncryptedTuple._1.toString, s"'${intEncryptedTuple._2}'")
        }
        case VarcharType =>
          Array("'" + encryptCharValue(v._2 stripPrefix "'" stripSuffix "'") + "'")
        case CharType =>
          Array("'" + encryptCharValue(v._2 stripPrefix "'" stripSuffix "'") + "'")
        case TextType =>
          Array("'" + encryptCharValue(v._2 stripPrefix "'" stripSuffix "'") + "'")
        case ImageType =>
          Array(v._2)	// no encryption
      }
    }
  }

  
  /*
   * Encrypt a database, table or field name.
   * Always encrypted with DET scheme.
   * */
  def anonymizeName (name: String): String = {
    encryptTextDet(name)
  }


  /*
   * Calculate the size of a DET scheme encrypted byte array / string,
   * given the plaintext length.
   * */
  def calculateDetCiphertextLenght (plaintextLength: Int): Int = {
    (plaintextLength / DetBlockSize + 1) * DetBlockSize + DetBlockSize
  }


  /*
   * Calculate the size of a Base64 string representation of a DET scheme ciphertext,
   * given the length of the plaintext String.
   *
   * WARNING: this assumes UTF-8 encoding, and that all String inputs will contain
   * only those characters that are represented in one byte.
   * e.g if the length of a string is x, the length of its bytes is also x
   * */
  def calculateBase64Length (length: Int): Int = {
    val ciphertextLength = calculateDetCiphertextLenght(length)
    (4.0 * Math.ceil(ciphertextLength / 3.0)).toInt
  }



/* ---------- FIELD NAME TRANSLATIONS --------------------------------------- */


  /* Prefix a field name for DET scheme */
  def translateDet (anonName: String): String = s"`$DetPrefix$anonName`"

  /* Prefix a field name for OPE scheme */
  def translateOpe (anonName: String): String = s"`$OpePrefix$anonName`"

  /* Prefix a field name for ADD scheme */
  def translateAdd (anonName: String): String = s"`$AddPrefix$anonName`"

  /* Prefix a field name for IMG (no scheme) */
  def translateImg (anonName: String): String = s"`$ImagePrefix$anonName`"

  /* Given an anonymized field name and a plaintext value,
   * return a Homomorphic Sum UDF call String */
  def translateSetAddUdfCall (anonName: String, value: String): String = {
    val addAnonName = translateAdd(anonName)
    s"$addAnonName = $SumUDF($addAnonName, '${encryptAdd(value)}', '${AddSecretKey.getNsquare.toString}')"
  }

  /*
   * Given an anonymized field name, return a Homomorphic
   * aggregate sum UDF call String */
  def translateAggAddUdfCall (anonName: String): String = {
    s"$AggSumUDF($anonName, '${AddSecretKey.getNsquare.toString}')"
  }

  /*
   * Given an anonymized name and a plaintext domain datatype, returns
   * an Array of the corresponding field names with encryption scheme prefixes.
   * */
  def translateFieldName (anonName: String, datatype: String): Array[String] = {
    datatype.toUpperCase match {
      case IntegerType =>
        Array(translateOpe(anonName), translateAdd(anonName))
      case VarcharType =>
        Array(translateDet(anonName))
      case CharType =>
        Array(translateDet(anonName))
      case TextType =>
        Array(translateDet(anonName))
	  case ImageType =>
		Array(translateImg(anonName))
    }
  }

  /*
   * Given anonymized table and field names, and the field's plaintext datatype,
   * matches the datatype to the default scheme for that datatype and returns a tuple
   * with the full field name (with scheme prefix) and the encryption scheme object.
   * */
  def translateDefaultTableFieldName (anonTableName: String, anonFieldName: String, datatype: String): (String, Scheme) = {
    datatype.toUpperCase match {
      case IntegerType =>
        (s"`$anonTableName`.`$OpePrefix$anonFieldName`", Ope)
      case VarcharType =>
        (s"`$anonTableName`.`$DetPrefix$anonFieldName`", Det)
      case CharType =>
        (s"`$anonTableName`.`$DetPrefix$anonFieldName`", Det)
      case TextType =>
        (s"`$anonTableName`.`$DetPrefix$anonFieldName`", Det)
      case ImageType =>
        (s"`$anonTableName`.`$ImagePrefix$anonFieldName`", NoScheme)
    }
  }

  /*
   * Given an anonymized field name and corresponding plaintext datatype,
   * matches the datatype to the default scheme for that datatype and returns a tuple
   * with the field name (with scheme prefix) and the encryption scheme object.

   * */
  def translateDefaultFieldName (anonFieldName: String, datatype: String): (String, Scheme) = {
    datatype.toUpperCase match {
      case IntegerType =>
        (s"`$OpePrefix$anonFieldName`", Ope)
      case VarcharType =>
        (s"`$DetPrefix$anonFieldName`", Det)
      case CharType =>
        (s"`$DetPrefix$anonFieldName`", Det)
      case TextType =>
        (s"`$DetPrefix$anonFieldName`", Det)
      case ImageType =>
        (s"`$ImagePrefix$anonFieldName`", NoScheme)
    }
  }


  /*
   * Anonymize the declaration of an SQL Integer datatype field.
   * Establishes the correct datatype and length parameter for each
   * of the supported encryption schemes.
   * */
  def translateIntegerDeclaration (anonName: String): Array[String] = {
    val opeDeclaration = s"${translateOpe(anonName)} $OpeDatatype"
    val addDeclaration = s"${translateAdd(anonName)} $AddDatatype($AddBlockSize)"
    Array(opeDeclaration, addDeclaration)
  }


  /*
   * Anonymize the declaration of an SQL text datatype field (Varchar/Char).
   * Establishes the correct datatype and length parameter for each
   * of the supported encryption schemes.
   * */
  def translateTextDeclaration (anonName: String, length: Int): Array[String] = {
    val detDeclaration = s"${translateDet(anonName)} $DetDatatype(" +
      calculateBase64Length(length) + ")"
    Array(detDeclaration)
  }

  def translateTextDeclarationNoLen (anonName: String): Array[String] = {
    val detTextDeclaration = s"${translateDet(anonName)} $DetTextDatatype"
    Array(detTextDeclaration)
  }

  def translateImageDeclaration (anonName: String): Array[String] = {
    val imageDeclaration = s"${translateImg(anonName)} $ImgDatatype"
    Array(imageDeclaration)
  }


  /*
   * Translates an anonymized field declaration according to its plaintext domain datatype.
   * */
  def translateDeclaration (anonName: String, datatype: String): Array[String] = {
    datatype match {
      case IntegerType =>
        translateIntegerDeclaration(anonName)
      case TextType =>
        translateTextDeclarationNoLen(anonName)
      case ImageType =>
        translateImageDeclaration(anonName)
    }
  }


  /*
   * Translates an anonymized field declaration according to its plaintext domain datatype.
   * */
  def translateDeclaration (anonName: String, datatype: String, length: Int): Array[String] = {
    datatype match {
      case VarcharType =>
        translateTextDeclaration(anonName, length)
      case CharType =>
        translateTextDeclaration(anonName, length)
    }
  }


}
