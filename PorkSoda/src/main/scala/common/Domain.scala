package common

/*
 * Domain definition and JSON support
 * */


import akka.actor.ActorRef
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._


/* ------------------------------------------------------------
 *
 * Domain case classes:
 *
 * ResultSet items to be transferred in JSON format
 * */
sealed trait RSData
final case class RSMeta (nCols: Int, cols: IndexedSeq[(String, String)]) extends RSData
final case class RSRow (cols: IndexedSeq[Array[Byte]]) extends RSData

/*
 * MIE results to be transferred in JSON format
 * */
sealed trait MIEData
final case class MIESearchResult (id: Int, score: Double) extends MIEData
final case class MIESearchResultArray (results: Array[MIESearchResult]) extends MIEData

/*
 * Messages exchanged to and between:
 *
 * RequestHandlerActor
 * */
sealed trait Request

sealed trait SQLRequest extends Request
final case class SQLConnect (db: String, user: String, password: String) extends SQLRequest
final case class SQLExecuteUpdate (query: String) extends SQLRequest
final case class SQLExecuteQuery (query: String, nCols: Int = 0) extends SQLRequest
final case class SQLSetAutoCommit (autoCommit: Boolean) extends SQLRequest
final case class SQLSetTransactionIsolation (level: Int) extends SQLRequest
final case object SQLCommit extends SQLRequest
final case object SQLRollback extends SQLRequest

sealed trait SQLResponse
final case class SQLSuccess(rows: Int) extends SQLResponse
final case class SQLFailure(exceptionCause: String) extends SQLResponse

sealed trait MIERequest extends Request
final case object MIEIndex extends MIERequest
final case class MIEAddImage (id: Int, bytes: Array[Byte]) extends MIERequest
final case class MIESearchImage (bytes: Array[Byte]) extends MIERequest
final case class MIEGetImage (id: Int) extends MIERequest

sealed trait MIEResponse
final case class MIESuccess (message: String) extends MIEResponse
final case class MIEFailure (message: String) extends MIEResponse
final case class MIEImage (bytes: Array[Byte], unzippedDataSize: Int = 0) extends MIEResponse

sealed trait MultimodalRequest extends Request
final case class MultimodalQuery (sqlQuery: SQLExecuteQuery, conjunction: String, imageField: String, mieSearch: MIESearchImage) extends MultimodalRequest

/*
 * ManagerActor
 * */
sealed trait SessionRequest
final case class Connect (request: SQLConnect) extends SessionRequest
final case class ConnectResponse (status: Boolean) extends SessionRequest
final case class Disconnect (id: Int) extends SessionRequest
final case class Forward (id: Int, request: Request) extends SessionRequest


/* ------------------------------------------------------------
 *
 * JSON support:
 *
 * */
object ProxyJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {

  // MIEData message formats
  implicit val mieSearchResult = jsonFormat2(MIESearchResult)
  implicit val mieSearchResultArray = jsonFormat1(MIESearchResultArray)

  // SQLRequest message formats
  implicit val sqlConnectFormats = jsonFormat3(SQLConnect)
  implicit val sqlExecuteUpdateFormats = jsonFormat1(SQLExecuteUpdate)
  implicit val sqlExecuteQueryFormats = jsonFormat2(SQLExecuteQuery)
  implicit val sqlSetAutoCommitFormats = jsonFormat1(SQLSetAutoCommit)
  implicit val sqlSetTransactionIsolation = jsonFormat1(SQLSetTransactionIsolation)

  // SQLResponse message formats
  implicit val sqlSuccessFormats = jsonFormat1(SQLSuccess)
  implicit val sqlFailureFormats = jsonFormat1(SQLFailure)

  // MIERequest message formats
  implicit val mieAddImageFormats = jsonFormat2(MIEAddImage)
  implicit val mieSearchImageFormats = jsonFormat1(MIESearchImage)
  implicit val mieGetImageFormats = jsonFormat1(MIEGetImage)

  // MIEResponse message formats
  implicit val mieSuccessFormats = jsonFormat1(MIESuccess)
  implicit val mieFailureFormats = jsonFormat1(MIEFailure)
  implicit val mieImageFormats = jsonFormat2(MIEImage)

  // MultimodalRequest message formats
  implicit val multimodalQueryFormats = jsonFormat4(MultimodalQuery)

  // RSData extended types formats
  implicit val rsMetaFormats = jsonFormat2(RSMeta)
  implicit val rsRowFormats = jsonFormat1(RSRow)


  // Offers support for marshalling RSData typed objects
  implicit object RSDataJsonFormat extends RootJsonFormat[RSData] {
    
    // RSData to JSON
    def write (data: RSData) = data match {
      case m: RSMeta => m.toJson
      case r: RSRow => r.toJson
    }

    // JSON to RSData
    def read (v: JsValue) = v match {
      // compiler will complain about this not being exhaustive
      case obj: JsObject =>
        // only two possibilities, RSMeta (2 fields) or RSRow (1 field)
        if (obj.fields.size == 2) v.convertTo[RSMeta]
        else v.convertTo[RSRow]
    }

  }

}
