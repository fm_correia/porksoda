package common 

import java.sql.{Connection, DriverManager, ResultSet, SQLException, Statement}


object DatabaseConnector {

  private final val Driver = "com.mysql.jdbc.Driver"
  private final val UrlPrefix = "jdbc:mysql://"
	private final val DefaultAddress: String = "localhost"

}


class DatabaseConnector {


  import DatabaseConnector._

  var connection: Connection = null
  var statement: Statement = null

  Class.forName(Driver)


  /*
   * Open a connection to the remote database.
   * Instantiate the statement for posterior use.
   * */
  @throws(classOf[SQLException])
  def establishConnection (databaseName: String, user: String, password: String) {
    try {
      connection = DriverManager.getConnection(s"$UrlPrefix$DefaultAddress/$databaseName", user, password)
      statement = connection.createStatement()
    }
    catch {
      case e: SQLException => throw e
    }
  }
  
  @throws(classOf[SQLException])
  def establishConnection (address: String, databaseName: String, user: String, password: String) {
    try {
      connection = DriverManager.getConnection(s"$UrlPrefix$address/$databaseName", user, password)
      statement = connection.createStatement()
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Close the current connection.
   * */
  @throws(classOf[SQLException])
  def closeConnection () {
    try {
      statement = null
      connection.close()
    }
    catch {
      case e: SQLException => throw e
    } 
  }


  /*
   *
   * */
  @throws(classOf[SQLException])
  def setAutoCommit (autoCommit: Boolean) {
    try {
      connection.setAutoCommit(autoCommit)
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   *
   * */
  @throws(classOf[SQLException])
  def setTransactionIsolation (level: Int) {
    try {
      connection.setTransactionIsolation(level)
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   *
   * */
  @throws(classOf[SQLException])
  def commit () {
    try {
      connection.commit
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   *
   * */
  @throws(classOf[SQLException])
  def rollback () {
    try {
      connection.rollback
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Issue a query for which a ResultSet is not returned.
   * CREATE/DROP DATABASE/TABLE, INSERT, DELETE, UPDATE, etc.
   * */
  @throws(classOf[SQLException])
  def executeUpdate (query: String): Int = {
    try {
      statement.executeUpdate(query)
    }
    catch {
      case e: SQLException => throw e
    }
  }


  /*
   * Issue a query for which a ResultSet is returned.
   * TODO: Return a handler for the ResultSet.
   * */
  @throws(classOf[SQLException])
  def executeQuery (query: String): ResultSet = {
    try {
      statement.executeQuery(query)
    }
    catch {
      case e: SQLException => throw e
    }
  }

}
