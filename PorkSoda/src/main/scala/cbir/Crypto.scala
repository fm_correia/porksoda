package cbir

import java.io.{File, FileInputStream, FileOutputStream, IOException, FileNotFoundException}
import java.security.{AlgorithmParameters, InvalidAlgorithmParameterException, InvalidKeyException, Key, NoSuchAlgorithmException, SecureRandom}
import java.security.spec.InvalidParameterSpecException
import javax.crypto.{BadPaddingException, Cipher, IllegalBlockSizeException, KeyGenerator, Mac, NoSuchPaddingException}
import javax.crypto.spec.IvParameterSpec
import mie.crypto.{CBIRDCipherKeySpec, CBIRDCipherParameterSpec, CBIRDParameterSpec}


object Crypto {

  private final val CBIRDCipher = "CBIRDWithSymmetricCipher"
  private final val CBIRDMac = "CBIRD"

}


import Crypto._

class Crypto (keyFilePath: String = "cbirFiles/key", ivFilePath: String = "cbirFiles/iv") {


  private var key: Key = null
  private var iv: IvParameterSpec = null
  private var cbirdParams: CBIRDCipherParameterSpec = null

  setup

  def generateKey: Key = {
    val keyGen = KeyGenerator.getInstance(CBIRDCipher)
    keyGen.generateKey
  }


  def generateIv: Array[Byte] = {
    val cipher = Cipher.getInstance(CBIRDCipher)
    val length = cipher.getBlockSize
    val bytes = new Array[Byte](length)
    val random = new SecureRandom
    random.nextBytes(bytes)
    bytes
  }


  def readKey (file: File) = {
    var in = new FileInputStream(file)
    val bytes = new Array[Byte](in.available)
    in.read(bytes)
    in.close
    key = new CBIRDCipherKeySpec(bytes)
  }


  def readIv (file: File) = {
    var in = new FileInputStream(file)
    val bytes = new Array[Byte](in.available)
    in.read(bytes)
    in.close
    iv = new IvParameterSpec(bytes)
  }


  def writeKey (file: File) = {
    var out = new FileOutputStream(file)
    out.write(key.getEncoded)
    out.close
  }


  def writeIv (bytes: Array[Byte], file: File) = {
    var out = new FileOutputStream(file)
    out.write(bytes)
    out.close
  }


  @throws(classOf[IOException])
  @throws(classOf[NoSuchAlgorithmException])
  @throws(classOf[NoSuchPaddingException])
  private def setup = {
    
    val keyFile = new File(keyFilePath)
    val ivFile = new File(ivFilePath)

    /* Read key from file, if file does not exist generate a new key
     * and store it in a new file */
    if (keyFile.exists)
      readKey(keyFile)
    if (key == null) {
      key = generateKey
      writeKey(keyFile)
    }

    /* Same for IV */
    if (ivFile.exists)
      readIv(ivFile)
    if (iv == null) {
      val ivBytes = generateIv
      writeIv(ivBytes, ivFile)
      iv = new IvParameterSpec(ivBytes)
    }
    
    cbirdParams = new CBIRDCipherParameterSpec(iv)

  }


  def encryptImage (plaintext: Array[Byte]): Array[Byte] = {
    try {
      val cipher = Cipher.getInstance(CBIRDCipher)
      cipher.init(Cipher.ENCRYPT_MODE, key, cbirdParams)
      cipher.doFinal(plaintext)
    }
    catch {
      case e: Throwable => e.printStackTrace
      return null
    }
  }


  def decryptImage (ciphertext: Array[Byte]): Array[Byte] = {
    try {
      val cipher = Cipher.getInstance(CBIRDCipher)
      cipher.init(Cipher.DECRYPT_MODE, key, cbirdParams)
      cipher.doFinal(ciphertext)
    }
    catch {
      case e: Throwable => e.printStackTrace
      return null
    }
  }


  def getImageFeatures (image: Array[Byte]): Array[Byte] = {
    try {
      val mac = Mac.getInstance(CBIRDMac)
      mac.init((key.asInstanceOf[CBIRDCipherKeySpec]).getCBIRSpec)
      mac.update(image)
      mac.doFinal
    }
    catch {
      case e: Throwable => e.printStackTrace
      return null
    }
  }


  def getImageFeaturesSize: Int = {
    try {
      val params1: AlgorithmParameters = cbirdParams.getCBIRParameters
      val params2: CBIRDParameterSpec = params1.getParameterSpec(classOf[CBIRDParameterSpec])
      params2.getM
    }
    catch {
      case e: Throwable => e.printStackTrace
      return -1
    }
  }


}
