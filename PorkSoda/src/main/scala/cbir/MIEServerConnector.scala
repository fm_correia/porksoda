package cbir

import scala.util.control.Breaks._
import scala.collection.mutable.MutableList

import java.net.{Socket, UnknownHostException}
import java.io.{IOException, InputStream, OutputStream}
import java.nio.{ByteBuffer, ByteOrder}

import common._


object MIEServerConnector {

  private final val ServerPort: Int = 9978
  private final val ServerIp: String = "127.0.0.1"

}


class MIEServerConnector {

  import MIEServerConnector._


  /*
   * Bytes already zipped at Client Proxy
   * */
  private def send (sock: Socket, bytes: Array[Byte]) = {
    val os = sock.getOutputStream
    os.write(bytes)
  }


  private def recv (is: InputStream, buffer: Array[Byte], length: Int) = {
    var r: Int = 0
    while (r < length) {
      r += is.read(buffer, r, length - r)
      if (r == 0) break
    }
  }


  private def receiveSearchResults (sock: Socket): Array[MIESearchResult] = {
    try {
      var results_size = new Array[Byte](4)
      val is = sock.getInputStream
      recv(is, results_size, results_size.size)
      var buffer = ByteBuffer.wrap(results_size)
      val nResults = buffer.getInt

      var tmp = new Array[Byte](12)
      buffer = ByteBuffer.wrap(tmp)
      (for (i <- 1 to nResults) yield {
        recv(is, tmp, tmp.size)
        val id = buffer.getInt
        var tmp_float: Long = 0
        val b = buffer.order
        if (b != ByteOrder.LITTLE_ENDIAN) {
          buffer.order(ByteOrder.LITTLE_ENDIAN)
          tmp_float = buffer.getLong
          buffer.order(b)
        }
        else tmp_float = buffer.getInt

        val f = java.lang.Double.longBitsToDouble(tmp_float)
        val result = new MIESearchResult(id, f)
        buffer.rewind
        result
      }).toArray
    }
    catch {
      case e: IOException => throw e
    }
  }


  /*
   * This method takes no input bytes, send a single char
   * as per the MIEServer communication protocol.
   * */
  def index = {
    val sock = new Socket(ServerIp, ServerPort)
    send(sock, Array('i'.toByte))
    sock.close
  }


  /*
   * Forward bytes to MIEServer
   * */
  def addImage (bytes: Array[Byte]) = {
    val sock = new Socket(ServerIp, ServerPort)
    send(sock, 'a'.toByte +: bytes)
    sock.close
  }


  /*
   * Forward bytes to MIEServer and receive the search results
   * */
  def search (bytes: Array[Byte]): Array[MIESearchResult] = {
    val sock = new Socket(ServerIp, ServerPort)
    send(sock, 's'.toByte +: bytes)
    val results = receiveSearchResults(sock)
    sock.close
    results
  }


  /*
   * Request an image to MIEServer identified by the given id
   * */
  def getImage (id: Int): MIEImage = {
    val sock = new Socket(ServerIp, ServerPort)
    var buffer = new Array[Byte](5)
    var bb = ByteBuffer.wrap(buffer, 1, buffer.size - 1)
    buffer(0) = 'g'.toByte
    bb.putInt(id)
    send(sock, buffer)
    
    // receive metadata
    buffer = new Array[Byte](16)
    val is = sock.getInputStream
    recv(is, buffer, buffer.size)
    bb = ByteBuffer.wrap(buffer)
    val zipSize = bb.getLong
    val dataSize = bb.getLong
    
    // receive data
    buffer = new Array[Byte](zipSize.toInt)
    recv(is, buffer, buffer.size)
    sock.close
    
    new MIEImage(buffer, dataSize.toInt)
  }

}
