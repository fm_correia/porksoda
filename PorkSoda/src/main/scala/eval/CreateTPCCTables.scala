package eval

import httpclient.Client


object CreateTPCCTables {

  def main (args: Array[String]) {
  
    if (args.size < 3) {
      println("usage: CreateTPCCTables proxyAddress databaseName username [password]")
      println("example: CreateTPCCTables localhost:5150 testdb1 root password12345")
      System.exit(1)
    }

    val address = args(0)
    val database = args(1)
    val username = args(2)
    val password = if (args.size > 3) args(3) else ""

    var client = new Client

    client.connect(address, "", username, password)
		client.executeUpdate(s"create database if not exists $database")
		client.executeUpdate(s"use $database")

    client.executeUpdate("drop table if exists warehouse")
    client.executeUpdate("""create table warehouse (
      |w_id INT NOT NULL,
      | w_name VARCHAR(10),
      | w_street_1 VARCHAR(20),
      | w_street_2 VARCHAR(20),
      | w_city VARCHAR(20),
      | w_state CHAR(2),
      | w_zip CHAR(9),
      | w_tax INT,
      | w_ytd INT)""".stripMargin.replaceAll("\n", ""))

    client.executeUpdate("drop table if exists district")
    client.executeUpdate("""create table district (
      |d_id INT NOT NULL,
      | d_w_id INT NOT NULL,
      | d_name VARCHAR(10),
      | d_street_1 VARCHAR(20),
      | d_street_2 VARCHAR(20),
      | d_city VARCHAR(20),
      | d_state CHAR(20),
      | d_zip CHAR(9),
      | d_tax INT,
      | d_ytd INT,
      | d_next_o_id INT)""".stripMargin.replaceAll("\n", ""))

    client.executeUpdate("drop table if exists customer")
    client.executeUpdate("""create table customer (
      |c_id INT NOT NULL,
      | c_d_id INT NOT NULL,
      | c_w_id INT NOT NULL,
      | c_first VARCHAR(16),
      | c_middle CHAR(2),
      | c_last VARCHAR(16),
      | c_street_1 VARCHAR(20),
      | c_street_2 VARCHAR(20),
      | c_city VARCHAR(20),
      | c_state CHAR(2),
      | c_zip CHAR(9),
      | c_phone CHAR(16),
      | c_since VARCHAR(50),
      | c_credit CHAR(2),
      | c_credit_lim INT,
      | c_discount INT,
      | c_balance INT,
      | c_ytd_payment INT,
      | c_payment_cnt INT,
      | c_delivery_cnt INT,
      | c_data TEXT)""".stripMargin.replaceAll("\n", ""))
    
    client.executeUpdate("drop table if exists history")
    client.executeUpdate("""create table history (
      |h_c_id INT,
      | h_c_d_id INT,
      | h_c_w_id INT,
      | h_d_id INT,
      | h_w_id INT,
      | h_date VARCHAR(50),
      | h_amount INT,
      | h_data VARCHAR(24))""".stripMargin.replaceAll("\n", ""))

    client.executeUpdate("drop table if exists new_orders")
    client.executeUpdate("""create table new_orders (
      |no_o_id INT NOT NULL,
      | no_d_id INT NOT NULL,
      | no_w_id INT NOT NULL)""".stripMargin.replaceAll("\n", ""))

    client.executeUpdate("drop table if exists orders")
    client.executeUpdate("""create table orders (
      |o_id INT NOT NULL,
      | o_d_id INT NOT NULL,
      | o_w_id INT NOT NULL,
      | o_c_id INT,
      | o_entry_d VARCHAR(50),
      | o_carrier_id INT,
      | o_ol_cnt INT,
      | o_all_local INT)""".stripMargin.replaceAll("\n", ""))

    client.executeUpdate("drop table if exists order_line")
    client.executeUpdate("""create table order_line (
      |ol_o_id INT NOT NULL,
      | ol_d_id INT NOT NULL,
      | ol_w_id INT NOT NULL,
      | ol_number INT NOT NULL,
      | ol_i_id INT,
      | ol_supply_w_id INT,
      | ol_delivery_d VARCHAR(50),
      | ol_quantity INT,
      | ol_amount INT,
      | ol_dist_info CHAR(24))""".stripMargin.replaceAll("\n", ""))

    client.executeUpdate("drop table if exists item")
    client.executeUpdate("""create table item (
      |i_id INT NOT NULL,
      | i_im_id INT,
      | i_name VARCHAR(24),
      | i_price INT,
      | i_data VARCHAR(50))""".stripMargin.replaceAll("\n", ""))

    client.executeUpdate("drop table if exists stock")
    client.executeUpdate("""create table stock (
      |s_i_id INT NOT NULL,
      | s_w_id INT NOT NULL,
      | s_quantity INT,
      | s_dist_01 CHAR(24),
      | s_dist_02 CHAR(24),
      | s_dist_03 CHAR(24),
      | s_dist_04 CHAR(24),
      | s_dist_05 CHAR(24),
      | s_dist_06 CHAR(24),
      | s_dist_07 CHAR(24),
      | s_dist_08 CHAR(24),
      | s_dist_09 CHAR(24),
      | s_dist_10 CHAR(24),
      | s_ytd INT,
      | s_order_cnt INT,
      | s_remote_cnt INT,
      | s_data VARCHAR(50))""".stripMargin.replaceAll("\n", ""))

    client.disconnect
  
  }

}
