package eval

import httpclient.Client
import scala.collection.mutable.ArrayBuffer
import java.util.Locale


object ImageSearchTest {


	var client = new Client


	def calcAvg (values: ArrayBuffer[Double]): Double = {
		values.foldLeft(0.0)(_ + _) / values.size
	}


	def calcStdDeviation (values: ArrayBuffer[Double], avg: Double): Double = {
		val distSquares = values.map(v => Math.pow((v - avg), 2))
		val sumOfSquares = distSquares.foldLeft(0.0)(_ + _)
		Math.sqrt(sumOfSquares / values.size)
	}


	def main (args: Array[String]) {
		
		var address = ""
		var user = ""
		var password = ""
		var imagesDir = ""
		var numImages = 0

		if (args.size < 10) {
			println("usage: eval.ImageSearchTest -a address -u user -d /path/to/image/set/ -n numImages")
			System.exit(1)
		}
		
		args.sliding(2, 2).foreach {
			case Array("-a", addr) => address = addr
			case Array("-u", usr) => user = usr
			case Array("-p", pass) => password = pass
			case Array("-d", dir) => imagesDir = dir
			case Array("-n", ni) => numImages = ni.toInt
		}

		client.connect(address, "", user, password)


/* ----- Start image upload ------------------------------------------------- */

		println(s"Starting image searching measurements, $numImages images...")

		var times = ArrayBuffer[Double]()

		for (i <- 0 to numImages) {
			val start = System.nanoTime
			client.searchImage(imagesDir, i)
			val end = System.nanoTime
			times += (end - start) / 1000000.0
			printf(".")
		}

		val avg = calcAvg(times)
		val stdDev = calcStdDeviation(times, avg)


		// to format Double prints with dot instead of comma
		Locale.setDefault(Locale.US)

		println()
		println("Image search tests completed.")
		println(f"AVERAGE: $avg%.4f ms\nSTDDEV: $stdDev%.4f ms")

		client.disconnect

	}

	
}
