package eval;

import httpclient._
import common.DatabaseConnector
import scala.util.Random
import scala.collection.mutable.ArrayBuffer
import java.util.Locale
import java.io.PrintWriter


object SQLTests {

	final val PeopleRows = 450		// number of columns for people table
	final val AccountsRows = 450	// number of rows for people table
	final val TripsRows = 1500			// number of rows for trips table
	final val NumRuns = 100			// number of runs for query performance tests


	final val Countries = Array("Portugal", "Spain", "France", "Italy", "Britain", "Greece", "Russia",
			"Morroco", "Egypt", "Turkey", "China", "Japan", "North_Korea", "Thailand", "India", "Mexico",
			"Brazil", "Paraguay", "Argentina", "Canada", "Hawaii", "Madagascar", "Azores", "North_Pole",
			"Mars", "Jamaica", "Ivory_Coast", "Cuba", "Zimbabwe")

	final val FirstNames = Array("Anacleto", "Ana", "Bruno", "Barbara", "Carlos", "Carla", "Daniel", "Diana",
			"Emanuel", "Estrudes", "Francisco", "Fernanda", "Guilherme", "Gabriela", "Horacio", "Helena", "Ivo", "Isabel",
			"Jorge", "Joana", "Leonardo", "Lucia", "Miguel", "Mara", "Nuno", "Natalia", "Onofre", "Odete", "Pedro", "Paula",
			"Quim", "Quiteria", "Roberto", "Rute", "Sergio", "Susana", "Telmo", "Tania", "Usaid", "Ursula", "Venancio", "Vania",
			"Xavier", "Xena", "Zacarias", "Zulmira")

	final val LastNames = Array("Alves", "Bastos", "Correia", "Dores", "Esteves", "Florencio", "Gomes", "Henriques",
			"Infante", "Jardim", "Lourenco", "Moura", "Nunes", "Oliveira", "Pacheco", "Quental", "Rocha", "Sousa", "Tavares",
			"Uchoa", "Valente", "Xavier", "Zarco")

	final val AccountTypes = Array("Savings", "Basic_Checking", "Interest_Bearing", "Money_Market_Deposit", "Certificate_Deposit")

	final val CreateTablePeople = "create table people (p_id INT NOT NULL, firstname VARCHAR(20), lastname VARCHAR(20), age INT, height INT, country VARCHAR(20))"
	final val CreateTableBankAccounts = "create table bank_accounts (a_id INT NOT NULL, p_id INT NOT NULL, type VARCHAR(20), balance INT)"
	final val CreateTableTrips = "create table trips (p_id INT NOT NULL, destination VARCHAR(20), month INT)"
	final val CreateTableTripsWithImages = "create table trips (p_id INT NOT NULL, destination VARCHAR(20), month INT, photo IMAGE)"

	final val MaxStartingBalance = 1000
	final val MaxAge = 101
	final val MaxHeight = 200
	final val Months = 12
	final val PhotoSetSize = 999


	def calcMedian (values: ArrayBuffer[Double]): Double = {
		if ((values.size % 2) == 0)
			return (values(values.size / 2) + values(values.size / 2 - 1)) / 2.0
		else
			return values(values.size / 2)
	}


	def calcStdDeviation (values: ArrayBuffer[Double], avg: Double): Double = {
		val distSquares = values.map(v => Math.pow((v - avg), 2))
		val sumOfSquares = distSquares.foldLeft(0.0)(_ + _)
		Math.sqrt(sumOfSquares / values.size)
	}


}


object PlaintextSQLTest {

	import SQLTests._

	val dc = new DatabaseConnector


	def measureQueryTime (query: String): Double = {
		val start = System.nanoTime
		val rs = dc.executeQuery(query)
		val end = System.nanoTime
		rs.close
		return (end - start) / 1000000.0
	}


	def measureUpdateTime (query: String): (Int, Double) = {
		val start = System.nanoTime
		val cols = dc.executeUpdate(query)
		val end = System.nanoTime
		return (cols, (end - start) / 1000000.0)
	}


	def main (args: Array[String]) = {
		
		if (args.size < 8) {
			println("usage: eval.EncryptedSQLTest -a address -u user -p password -P (y/n)")
			System.exit(1)
		}

		val database = "encryptedSQL"
		var address: String = ""
		var user: String = ""
		var password: String = ""
		var populate: Boolean = true

		args.sliding(2, 2).foreach {
			case Array("-a", addr) => address = addr
			case Array("-u", usr) => user = usr
			case Array("-p", pass) => password = pass
			case Array("-P", pop) => if (pop equals 'y') populate = true else populate = false
		}

		val r = new Random
		dc.establishConnection(address, "", user, password)

		dc.executeUpdate(s"drop database if exists $database")
		dc.executeUpdate(s"create database $database")
		dc.executeUpdate(s"use $database")

		dc.executeUpdate(CreateTablePeople)
		dc.executeUpdate(CreateTableBankAccounts)
		dc.executeUpdate(CreateTableTrips)


/* ----- Populate tables ---------------------------------------------------- */


		/* Insert people data */
		println("Populating people table...")
		for (i <- 0 to PeopleRows - 1) {
			val pid = i + 1
			val fname = FirstNames(r.nextInt(FirstNames.size))
			val lname = LastNames(r.nextInt(LastNames.size))
			val age = r.nextInt(MaxAge)
			val height = r.nextInt(MaxHeight)
			val country = Countries(r.nextInt(Countries.size))
			dc.executeUpdate(s"insert into people (p_id, firstname, lastname, age, height, country) values ($pid, '$fname', '$lname', $age, $height, '$country')")
		}

		/* Insert accounts data */
		println("Populating accounts table...")
		for (i <- 0 to AccountsRows - 1) {
			val aid = i + 1
			val pid = r.nextInt(PeopleRows)
			val atype = AccountTypes(r.nextInt(AccountTypes.size))
			val balance = r.nextInt(MaxStartingBalance)
			dc.executeUpdate(s"insert into bank_accounts (a_id, p_id, type, balance) values ($aid, $pid, '$atype', $balance)")
		}

		/* Insert trips data */
		println("Populating trips table...")
		for (i <- 0 to TripsRows - 1) {
			val pid = r.nextInt(PeopleRows)
			val destination = Countries(r.nextInt(Countries.size))
			val month = r.nextInt(Months)
			dc.executeUpdate(s"insert into trips (p_id, destination, month) values ($pid, '$destination', $month)")
		}


/* ----- Perform queries ---------------------------------------------------- */


		var eqSelectTimes = ArrayBuffer[Double]()
		var rangeSelectTimes = ArrayBuffer[Double]()
		var joinTimes = ArrayBuffer[Double]()
		var sumTimes = ArrayBuffer[Double]()
		var deleteTimes = ArrayBuffer[Double]()
		var insertTimes = ArrayBuffer[Double]()
		var updateSetTimes = ArrayBuffer[Double]()
		var updateIncTimes = ArrayBuffer[Double]()

		var start: Double = 0.0
		var end: Double = 0.0

		println (s"Starting query performance measurements, $NumRuns runs...")

		for (i <- 0 to NumRuns - 1) {

			try {

				var pid = r.nextInt(PeopleRows)
				var country = Countries(r.nextInt(Countries.size))
				var lname = LastNames(r.nextInt(LastNames.size))
				var age = r.nextInt(MaxAge)
				var height = r.nextInt(MaxHeight)
				var month = r.nextInt(Months - Months / 2)
				var balance = r.nextInt(MaxStartingBalance)
				var atype = AccountTypes(r.nextInt(AccountTypes.size))
				var aid = r.nextInt(AccountsRows)

				eqSelectTimes += measureQueryTime(s"select firstname, lastname from people where p_id = $pid")
				eqSelectTimes += measureQueryTime(s"select p_id from people where country = '$country' and lastname = '$lname'")
				eqSelectTimes += measureQueryTime(s"select type from bank_accounts where p_id = $pid")
				eqSelectTimes += measureQueryTime(s"select destination from trips where month = $month")

				rangeSelectTimes += measureQueryTime(s"select p_id from people where country = '$country' and age < $age")
				rangeSelectTimes += measureQueryTime(s"select lastname from people where age > $age and height < $height")
				rangeSelectTimes += measureQueryTime(s"select p_id from bank_accounts where balance > $balance")
				rangeSelectTimes += measureQueryTime(s"select max(balance) from bank_accounts")

				joinTimes += measureQueryTime(s"select people.firstname, people.lastname, bank_accounts.balance from people join bank_accounts on people.p_id = bank_accounts.p_id where bank_accounts.balance < $balance")
				joinTimes += measureQueryTime(s"select people.age, trips.destination from people join trips on people.p_id = trips.p_id where people.height > $height and trips.destination = '$country'")
				joinTimes += measureQueryTime(s"select bank_accounts.p_id, bank_accounts.type from bank_accounts join trips on bank_accounts.p_id = trips.p_id where bank_accounts.type = '$atype'")

				sumTimes += measureQueryTime(s"select sum(balance) from bank_accounts")
				sumTimes += measureQueryTime(s"select sum(balance) from bank_accounts where p_id = $pid or a_id = $aid")
				sumTimes += measureQueryTime(s"select sum(age) from people where country = '$country'")
				sumTimes += measureQueryTime(s"select sum(month) from trips")

				var updateRet: (Int, Double) = (0, 0.0)
				while (updateRet._1 < 1) {
					month = r.nextInt(Months)
					country = Countries(r.nextInt(Countries.size))
					updateRet = measureUpdateTime(s"delete from trips where month = $month and destination = '$country'")
				}
				deleteTimes += updateRet._2

				for (i <- 1 to updateRet._1) {
					pid = r.nextInt(PeopleRows)
					country = Countries(r.nextInt(Countries.size))
					month = r.nextInt(Months)
					insertTimes += measureUpdateTime(s"insert into trips (p_id, destination, month) values ($pid, '$country', $month)")._2
				}

				updateSetTimes += measureUpdateTime(s"update bank_accounts set balance = ${MaxStartingBalance / 2} where type = '$atype'")._2
				updateSetTimes += measureUpdateTime(s"update trips set destination = '$country' where month = $month and p_id = $pid")._2

				updateIncTimes += measureUpdateTime(s"update bank_accounts set balance = balance + 500 where a_id = $aid or p_id = $pid")._2
				updateIncTimes += measureUpdateTime(s"update bank_accounts set balance = balance + 50 where type = '$atype'")._2
				if ((i % 10) == 0)
					updateIncTimes += measureUpdateTime(s"update people set age = age + 1, height = height + 1")._2

			}
			catch {
				case e: Throwable =>
					e.printStackTrace
			}
			
			printf(".")
			if ((i > 0) && (i % (NumRuns / 10)) == 0) println(s" $i")

		}
		
		println(s". ${NumRuns}\n")

		
		val eqAvg = (eqSelectTimes.foldLeft(0.0)(_ + _)) / eqSelectTimes.size
		val rangeAvg = (rangeSelectTimes.foldLeft(0.0)(_ + _)) / rangeSelectTimes.size
		val joinAvg = (joinTimes.foldLeft(0.0)(_ + _)) / joinTimes.size
		val sumAvg = (sumTimes.foldLeft(0.0)(_ + _)) / sumTimes.size
		val deleteAvg = (deleteTimes.foldLeft(0.0)(_ + _)) / deleteTimes.size
		val insertAvg = (insertTimes.foldLeft(0.0)(_ + _)) / insertTimes.size
		val updateSetAvg = (updateSetTimes.foldLeft(0.0)(_ + _)) / updateSetTimes.size
		val updateIncAvg = (updateIncTimes.foldLeft(0.0)(_ + _)) / updateIncTimes.size

/*
		val eqMedian = calcMedian(eqSelectTimes.sorted)
		val rangeMedian = calcMedian(rangeSelectTimes.sorted)
		val joinMedian = calcMedian(joinTimes.sorted)
		val sumMedian = calcMedian(sumTimes.sorted)
		val deleteMedian = calcMedian(deleteTimes.sorted)
		val insertMedian = calcMedian(insertTimes.sorted)
		val updateSetMedian = calcMedian(updateSetTimes.sorted)
		val updateIncMedian = calcMedian(updateIncTimes.sorted)
*/

		val eqSD = calcStdDeviation(eqSelectTimes, eqAvg)
		val rangeSD = calcStdDeviation(rangeSelectTimes, rangeAvg)
		val joinSD = calcStdDeviation(joinTimes, joinAvg)
		val sumSD = calcStdDeviation(sumTimes, sumAvg)
		val deleteSD = calcStdDeviation(deleteTimes, deleteAvg)
		val insertSD = calcStdDeviation(insertTimes, insertAvg)
		val updateSetSD = calcStdDeviation(updateSetTimes, updateSetAvg)
		val updateIncSD = calcStdDeviation(updateIncTimes, updateIncAvg)

		
		// to format Double prints with dot instead of comma
		Locale.setDefault(Locale.US)
		
/*
		println(f"SelectEq:\tavg: $eqAvg%.4f ms; median: $eqMedian%.4f ms; stdDev: $eqSD%.4f ms")
		println(f"SelectRange:\tavg: $rangeAvg%.4f ms; median: $rangeMedian%.4f ms; stdDev: $rangeSD%.4f ms")
		println(f"Join:\t\tavg: $joinAvg%.4f ms; median: $joinMedian%.4f ms; stdDev: $joinSD%.4f ms")
		println(f"Sum:\t\tavg: $sumAvg%.4f ms; median: $sumMedian%.4f ms; stdDev: $sumSD%.4f ms")
		println(f"Delete:\t\tavg: $deleteAvg%.4f ms; median: $deleteMedian%.4f ms; stdDev: $deleteSD%.4f ms")
		println(f"Insert:\t\tavg: $insertAvg%.4f ms; median: $insertMedian%.4f ms; stdDev: $insertSD%.4f ms")
		println(f"UpdateSet:\tavg: $updateSetAvg%.4f ms; median: $updateSetMedian%.4f ms; stdDev: $updateSetSD%.4f ms")
		println(f"UpdateInc:\tavg: $updateIncAvg%.4f ms; median: $updateIncMedian%.4f ms; stdDev: $updateIncSD%.4f ms")
*/

		val pw = new PrintWriter("eval_outputs/plaintext_query_types")

		pw.println(f"$eqAvg%.3f $eqSD%.3f")
		pw.println(f"$rangeAvg%.3f $rangeSD%.3f")
		pw.println(f"$joinAvg%.3f $joinSD%.3f")
		pw.println(f"$sumAvg%.3f $sumSD%.3f")
		pw.println(f"$deleteAvg%.3f $deleteSD%.3f")
		pw.println(f"$insertAvg%.3f $insertSD%.3f")
		pw.println(f"$updateSetAvg%.3f $updateSetSD%.3f")
		pw.println(f"$updateIncAvg%.3f $updateIncSD%.3f")

		pw.close
		
		dc.closeConnection

	}
	
}


object EncryptedSQLTest {

	import SQLTests._

	var client: Client = new Client
	var imgDir = ""


	def measureQueryTime (query: String): Double = {
		val start = System.nanoTime
		val rs = client.executeQuery(query)
		val end = System.nanoTime
		while (rs.next) { /* consume entire stream (necessary) */ }
		return (end - start) / 1000000.0
	}


	def measureUpdateTime (query: String): (Int, Double) = {
		val start = System.nanoTime
		val cols = client.executeUpdate(query)
		val end = System.nanoTime
		return (cols, (end - start) / 1000000.0)
	}


	def measureMultimodalQueryTime (query: String, conjunction: String, imgField: String, imgNumber: Int): Double = {
		val start = System.nanoTime
		val rs = client.executeMultimodalQuery(query, conjunction, imgField, imgDir, imgNumber)
		while (rs.next) { /* consume entire stream (necessary) */ }
		val end = System.nanoTime
		return (end - start) / 1000000.0
	}


	def main (args: Array[String]) = {
		
		if (args.size < 8) {
			println("usage: eval.EncryptedSQLTest -a address -u user -p password -P (y/n) -i /path/to/images/dir/")
			System.exit(1)
		}

		val database = "localEncryptedSQL"
		var address: String = ""
		var user: String = ""
		var password: String = ""
		var populate: Boolean = true

		args.sliding(2, 2).foreach {
			case Array("-a", addr) => address = addr
			case Array("-u", usr) => user = usr
			case Array("-p", pass) => password = pass
			case Array("-P", pop) => if (pop equals "y") populate = true else populate = false
			case Array("-i", path) => imgDir = path
		}

		val r = new Random
		client.connect(address, database, user, password)


		if (populate) {

			client.executeUpdate(s"drop database if exists $database")
			client.executeUpdate(s"create database $database")
			client.executeUpdate(s"use $database")

			client.executeUpdate(CreateTablePeople)
			client.executeUpdate(CreateTableBankAccounts)
			client.executeUpdate(CreateTableTripsWithImages)


/* ----- Populate tables ---------------------------------------------------- */


			/* Insert people data */
			println("Populating people table...")
			for (i <- 0 to PeopleRows - 1) {
				val pid = i + 1
				val fname = FirstNames(r.nextInt(FirstNames.size))
				val lname = LastNames(r.nextInt(LastNames.size))
				val age = r.nextInt(MaxAge)
				val height = r.nextInt(MaxHeight)
				val country = Countries(r.nextInt(Countries.size))
				try {
					client.executeUpdate(s"insert into people (p_id, firstname, lastname, age, height, country) values ($pid, '$fname', '$lname', $age, $height, '$country')")
				}
				catch {
					case e: Throwable =>
						e.printStackTrace
				}
			}

			/* Insert accounts data */
			println("Populating accounts table...")
			for (i <- 0 to AccountsRows - 1) {
				val aid = i + 1
				val pid = r.nextInt(PeopleRows)
				val atype = AccountTypes(r.nextInt(AccountTypes.size))
				val balance = r.nextInt(MaxStartingBalance)
				try {
					client.executeUpdate(s"insert into bank_accounts (a_id, p_id, type, balance) values ($aid, $pid, '$atype', $balance)")
				}
				catch {
					case e: Throwable =>
						e.printStackTrace
				}
			}

			/* Insert trips data */
			println("Populating trips table...")
			for (i <- 0 to TripsRows - 1) {
				val pid = r.nextInt(PeopleRows)
				val destination = Countries(r.nextInt(Countries.size))
				val month = r.nextInt(Months)
				val photo = r.nextInt(PhotoSetSize)
				try {
					client.executeUpdate(s"insert into trips (p_id, destination, month, photo) values ($pid, '$destination', $month, $photo)")
				}
				catch {
					case e: Throwable =>
						e.printStackTrace
				}
			}
		}


/* ----- Perform queries ---------------------------------------------------- */


		var eqSelectTimes = ArrayBuffer[Double]()
		var rangeSelectTimes = ArrayBuffer[Double]()
		var joinTimes = ArrayBuffer[Double]()
		var sumTimes = ArrayBuffer[Double]()
		var deleteTimes = ArrayBuffer[Double]()
		var insertTimes = ArrayBuffer[Double]()
		var updateSetTimes = ArrayBuffer[Double]()
		var updateIncTimes = ArrayBuffer[Double]()
		var multimodalTimes = ArrayBuffer[Double]()

		var start: Double = 0.0
		var end: Double = 0.0

		println (s"Starting query performance measurements, $NumRuns runs...")

		for (i <- 0 to NumRuns - 1) {

			try {

				var pid = r.nextInt(PeopleRows)
				var country = Countries(r.nextInt(Countries.size))
				var lname = LastNames(r.nextInt(LastNames.size))
				var age = r.nextInt(MaxAge)
				var height = r.nextInt(MaxHeight)
				var month = r.nextInt(Months - Months / 2)
				var balance = r.nextInt(MaxStartingBalance)
				var atype = AccountTypes(r.nextInt(AccountTypes.size))
				var aid = r.nextInt(AccountsRows)

				//println("select eq")
				eqSelectTimes += measureQueryTime(s"select firstname, lastname from people where p_id = $pid")
				eqSelectTimes += measureQueryTime(s"select p_id from people where country = '$country' and lastname = '$lname'")
				eqSelectTimes += measureQueryTime(s"select type from bank_accounts where p_id = $pid")
				eqSelectTimes += measureQueryTime(s"select destination from trips where month = $month")

				//println("select range")
				rangeSelectTimes += measureQueryTime(s"select p_id from people where country = '$country' and age < $age")
				rangeSelectTimes += measureQueryTime(s"select lastname from people where age > $age and height < $height")
				rangeSelectTimes += measureQueryTime(s"select p_id from bank_accounts where balance > $balance")
				rangeSelectTimes += measureQueryTime(s"select max(balance) from bank_accounts")

				//println("join")
				joinTimes += measureQueryTime(s"select people.firstname, people.lastname, bank_accounts.balance from people join bank_accounts on people.p_id = bank_accounts.p_id where bank_accounts.balance < $balance")
				joinTimes += measureQueryTime(s"select people.age, trips.destination from people join trips on people.p_id = trips.p_id where people.height > $height and trips.destination = '$country'")
				joinTimes += measureQueryTime(s"select bank_accounts.p_id, bank_accounts.type from bank_accounts join trips on bank_accounts.p_id = trips.p_id where bank_accounts.type = '$atype'")

				//println("sum")
				sumTimes += measureQueryTime(s"select sum(balance) from bank_accounts")
				sumTimes += measureQueryTime(s"select sum(balance) from bank_accounts where p_id = $pid or a_id = $aid")
				sumTimes += measureQueryTime(s"select sum(age) from people where country = '$country'")
				sumTimes += measureQueryTime(s"select sum(month) from trips")

				//println("delete")
				var updateRet: (Int, Double) = (0, 0.0)
				while (updateRet._1 < 1) {
					updateRet = (0, 0.0)
					month = r.nextInt(Months)
					country = Countries(r.nextInt(Countries.size))
					updateRet = measureUpdateTime(s"delete from trips where month = $month and destination = '$country'")
				}
				deleteTimes += updateRet._2

				//println("insert")
				for (i <- 1 to updateRet._1) {
					pid = r.nextInt(PeopleRows)
					country = Countries(r.nextInt(Countries.size))
					month = r.nextInt(Months)
					insertTimes += measureUpdateTime(s"insert into trips (p_id, destination, month, photo) values ($pid, '$country', $month, ${r.nextInt(PhotoSetSize)})")._2
				}

				//println("update")
				updateSetTimes += measureUpdateTime(s"update bank_accounts set balance = ${MaxStartingBalance / 2} where type = '$atype'")._2
				updateSetTimes += measureUpdateTime(s"update trips set destination = '$country' where month = $month and p_id = $pid")._2

				updateIncTimes += measureUpdateTime(s"update bank_accounts set balance = balance + 500 where a_id = $aid or p_id = $pid")._2
				updateIncTimes += measureUpdateTime(s"update bank_accounts set balance = balance + 50 where type = '$atype'")._2
				if ((i % 10) == 0)
					updateIncTimes += measureUpdateTime(s"update people set age = age + 1, height = height + 1")._2


				if (imgDir != "") {
					//println("select img")
					country = Countries(r.nextInt(Countries.size))
					multimodalTimes += measureMultimodalQueryTime(
						s"select p_id, photo from trips where destination = '$country'", "AND", "photo", r.nextInt(PhotoSetSize))
					multimodalTimes += measureMultimodalQueryTime(
						s"select destination, photo from trips where p_id = $pid", "OR", "photo", r.nextInt(PhotoSetSize))
					multimodalTimes += measureMultimodalQueryTime(
						s"select month, photo from trips", "SINGLECLAUSE", "photo", r.nextInt(PhotoSetSize))
				}

			}
			catch {
				case e: Throwable =>
					e.printStackTrace
			}
			
			printf(".")
			if ((i > 0) && (i % (NumRuns / 10)) == 0) println(s" $i")

		}
		
		println(s". ${NumRuns}\n")

		
		val eqAvg = (eqSelectTimes.foldLeft(0.0)(_ + _)) / eqSelectTimes.size
		val rangeAvg = (rangeSelectTimes.foldLeft(0.0)(_ + _)) / rangeSelectTimes.size
		val joinAvg = (joinTimes.foldLeft(0.0)(_ + _)) / joinTimes.size
		val sumAvg = (sumTimes.foldLeft(0.0)(_ + _)) / sumTimes.size
		val deleteAvg = (deleteTimes.foldLeft(0.0)(_ + _)) / deleteTimes.size
		val insertAvg = (insertTimes.foldLeft(0.0)(_ + _)) / insertTimes.size
		val updateSetAvg = (updateSetTimes.foldLeft(0.0)(_ + _)) / updateSetTimes.size
		val updateIncAvg = (updateIncTimes.foldLeft(0.0)(_ + _)) / updateIncTimes.size
		val multimodalAvg = (multimodalTimes.foldLeft(0.0)(_ + _)) / multimodalTimes.size
/*
		val eqMedian = calcMedian(eqSelectTimes.sorted)
		val rangeMedian = calcMedian(rangeSelectTimes.sorted)
		val joinMedian = calcMedian(joinTimes.sorted)
		val sumMedian = calcMedian(sumTimes.sorted)
		val deleteMedian = calcMedian(deleteTimes.sorted)
		val insertMedian = calcMedian(insertTimes.sorted)
		val updateSetMedian = calcMedian(updateSetTimes.sorted)
		val updateIncMedian = calcMedian(updateIncTimes.sorted)
		val multimodalMedian = calcMedian(multimodalTimes.sorted)
*/
		val eqSD = calcStdDeviation(eqSelectTimes, eqAvg)
		val rangeSD = calcStdDeviation(rangeSelectTimes, rangeAvg)
		val joinSD = calcStdDeviation(joinTimes, joinAvg)
		val sumSD = calcStdDeviation(sumTimes, sumAvg)
		val deleteSD = calcStdDeviation(deleteTimes, deleteAvg)
		val insertSD = calcStdDeviation(insertTimes, insertAvg)
		val updateSetSD = calcStdDeviation(updateSetTimes, updateSetAvg)
		val updateIncSD = calcStdDeviation(updateIncTimes, updateIncAvg)
		val multimodalSD = calcStdDeviation(multimodalTimes, multimodalAvg)

		
		// to format Double prints with dot instead of comma
		Locale.setDefault(Locale.US)

/*
		println(f"SelectEq:\tavg: $eqAvg%.4f ms; median: $eqMedian%.4f ms; stdDev: $eqSD%.4f ms")
		println(f"SelectRange:\tavg: $rangeAvg%.4f ms; median: $rangeMedian%.4f ms; stdDev: $rangeSD%.4f ms")
		println(f"Join:\t\tavg: $joinAvg%.4f ms; median: $joinMedian%.4f ms; stdDev: $joinSD%.4f ms")
		println(f"Sum:\t\tavg: $sumAvg%.4f ms; median: $sumMedian%.4f ms; stdDev: $sumSD%.4f ms")
		println(f"Delete:\t\tavg: $deleteAvg%.4f ms; median: $deleteMedian%.4f ms; stdDev: $deleteSD%.4f ms")
		println(f"Insert:\t\tavg: $insertAvg%.4f ms; median: $insertMedian%.4f ms; stdDev: $insertSD%.4f ms")
		println(f"UpdateSet:\tavg: $updateSetAvg%.4f ms; median: $updateSetMedian%.4f ms; stdDev: $updateSetSD%.4f ms")
		println(f"UpdateInc:\tavg: $updateIncAvg%.4f ms; median: $updateIncMedian%.4f ms; stdDev: $updateIncSD%.4f ms")
		println(f"Multimodal:\tavg: $multimodalAvg%.4f ms; median: $multimodalMedian%.4f ms; stdDev: $multimodalSD%.4f ms")
*/

		val pw = new PrintWriter("eval_outputs/encrypted_query_types")

		pw.println(f"$eqAvg%.3f $eqSD%.3f")
		pw.println(f"$rangeAvg%.3f $rangeSD%.3f")
		pw.println(f"$joinAvg%.3f $joinSD%.3f")
		pw.println(f"$sumAvg%.3f $sumSD%.3f")
		pw.println(f"$deleteAvg%.3f $deleteSD%.3f")
		pw.println(f"$insertAvg%.3f $insertSD%.3f")
		pw.println(f"$updateSetAvg%.3f $updateSetSD%.3f")
		pw.println(f"$updateIncAvg%.3f $updateIncSD%.3f")
		pw.println(f"$multimodalAvg%.3f $multimodalSD%.3f")
		
		pw.close

		client.disconnect

	}
	
}
