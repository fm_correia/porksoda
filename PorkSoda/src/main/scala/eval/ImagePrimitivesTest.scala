package eval

import common.Util
import cbir.Crypto
import scala.collection.mutable.ArrayBuffer
import java.security.Security
import org.bouncycastle.jce.provider.BouncyCastleProvider
import mie.crypto.MIEProvider

import java.nio.ByteBuffer

object ImagePrimitivesTest {

	def main (args: Array[String]) = {
		
		Security.addProvider(new BouncyCastleProvider)
		Security.addProvider(new MIEProvider)

		var mieCrypto: Crypto = new Crypto

		var imagesDir = ""
		var encryptedDir = ""
		var numImages = 0

		if (args.size < 6) {
			println("usage: eval.ImagePrimitivesTest -d /path/to/image/set/ -e /path/to/encrypted/images/ -n numImages")
			System.exit(1)
		}
		
		args.sliding(2, 2).foreach {
			case Array("-d", dir) => imagesDir = dir
			case Array ("-e", dir) => encryptedDir = dir
			case Array("-n", ni) => numImages = ni.toInt
		}


		var encryptionTimes = new ArrayBuffer[Double]()
		var decryptionTimes = new ArrayBuffer[Double]()
		var featureExtractionTimes = new ArrayBuffer[Double]()

		for (i <- 0 to numImages) {
			val plainBytes = Util.readImg(imagesDir, i)

			val data = Util.readEncImg(encryptedDir, i)
			var bb = ByteBuffer.wrap(data, 0, 8)
			val imageSize = bb.getInt
			// next getInt would get text size
			var encryptedImageBytes = new Array[Byte](imageSize)
			Array.copy(data, 8, encryptedImageBytes, 0, imageSize)

			var start = System.nanoTime
			mieCrypto.encryptImage(plainBytes)
			var end = System.nanoTime
			encryptionTimes += (end - start) / 1000000.0

			start = System.nanoTime
			mieCrypto.decryptImage(encryptedImageBytes)
			end = System.nanoTime
			decryptionTimes += (end - start) / 1000000.0

			start = System.nanoTime
			mieCrypto.getImageFeatures(plainBytes)
			end = System.nanoTime
			featureExtractionTimes += (end - start) / 1000000.0

			print(".")
		}

		val avgEnc = (encryptionTimes.foldLeft(0.0)(_ + _)) / encryptionTimes.size
		val decAvg = (decryptionTimes.foldLeft(0.0)(_ + _)) / decryptionTimes.size
		val feaAvg = (featureExtractionTimes.foldLeft(0.0)(_ + _)) / featureExtractionTimes.size

		println(f"EncAvg: $avgEnc%.4f ms")
		println(f"DecAvg: $decAvg%.4f ms")
		println(f"FeatureAVg: $feaAvg%.4f ms")

		
	}

	
}
