package eval

import httpclient.Client
import java.util.Locale


object ImageUploadTest {
	
	var client = new Client

	def main (args: Array[String]) {
		
		var address = ""
		var user = ""
		var password = ""
		var imagesDir = ""
		var numImages = 0

		if (args.size < 10) {
			println("usage: eval.ImageUploadTest -a address -u user -p password -d /path/to/image/set/ -n numImages")
			System.exit(1)
		}

		args.sliding(2, 2).foreach {
			case Array("-a", addr) => address = addr
			case Array("-u", usr) => user = usr
			case Array("-p", pass) => password = pass
			case Array("-d", dir) => imagesDir = dir
			case Array("-n", ni) => numImages = ni.toInt
		}

		client.connect(address, "", user, password)


/* ----- Start image upload ------------------------------------------------- */

		println(s"Starting image upload, $numImages images...")

		val start = System.nanoTime
		for (i <- 0 to numImages) {
			client.addImage(imagesDir, i)
			printf(".")
		}
		val end = System.nanoTime
		val elapsed = (end - start) / 1000000000.0

		// to format Double prints with dot instead of comma
		Locale.setDefault(Locale.US)

		println()
		println("Image upload completed.")
		println(f"$elapsed%.4f seconds")

		/* Indexing time is measured in MIEServer */
		client.indexImages
		client.disconnect

	}
	
}
