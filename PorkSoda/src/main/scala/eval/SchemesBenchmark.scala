package eval

import scala.util.Random
import queryProcessing.QueryCryptoUtils
import java.io.PrintWriter
import java.util.Locale


object SchemesBenchmark {


	def calcThroughput (bytes: Int, ms: Double): Double = {
		(1000.0 * (bytes / 1024.0)) / ms
	}


	def main (args: Array[String]) = {

		val numOps = 10000
		val numWords = 1000
		val integerDomain = 1000
		val wordLength = 1024
		val tenPowerSix = 1000000.0

		val qc = new QueryCryptoUtils
		val r = new Random

		val words = Array.fill(numWords)(new String(Array.fill(wordLength)(r.nextPrintableChar)))
		val integerSet = Array.fill(numOps)(r.nextInt(integerDomain))
		val wordSet = Array.fill(numOps)(words(r.nextInt(numWords)))
		val integerStringSet = integerSet.map(_.toString)

		Locale.setDefault(Locale.US)

		/* ----- Encryption tests ----- */

		val pwOpe = new PrintWriter("eval_outputs/opeEncTimes")
		val (opeEncSet, opeEncTimes: IndexedSeq[Double]) = (for (i <- 0 to numOps - 1) yield {
			val start = System.nanoTime
			val enc = qc.encryptOpe(integerSet(i))
			val end = System.nanoTime
			val elapsed = (end - start) / tenPowerSix
			pwOpe.println(f"$elapsed%.3f")
			(enc, elapsed)
		}) unzip

		pwOpe.close

		val pwDet = new PrintWriter("eval_outputs/detEncTimes")
		val (detEncSet, detEncTimes: IndexedSeq[Double]) = (for (i <- 0 to numOps - 1) yield {
			val start = System.nanoTime
			val enc = qc.encryptTextDet(wordSet(i))
			val end = System.nanoTime
			val elapsed = (end - start) / tenPowerSix
			pwDet.println(f"$elapsed%.3f")
			(enc, elapsed)
		}) unzip

		pwDet.close

		val pwAdd = new PrintWriter("eval_outputs/addEncTimes")
		val (addEncSet, addEncTimes: IndexedSeq[Double]) = (for (i <- 0 to numOps - 1) yield {
			val start = System.nanoTime
			val enc = qc.encryptAdd(integerStringSet(i))
			val end = System.nanoTime
			val elapsed = (end - start) / tenPowerSix
			pwAdd.println(f"$elapsed%.3f")
			(enc, elapsed)
		}) unzip

		pwAdd.close


		/* ----- Decryption tests ----- */

		val opeDecTimes: IndexedSeq[Double] = for (i <- 0 to numOps - 1) yield {
			val start = System.nanoTime
			qc.decryptOpe(opeEncSet(i))
			val end = System.nanoTime
			(end - start) / tenPowerSix
		}

		val detDecTimes: IndexedSeq[Double] = for (i <- 0 to numOps - 1) yield {
			val start = System.nanoTime
			qc.decryptTextDet(detEncSet(i))
			val end = System.nanoTime
			(end - start) / tenPowerSix
		}

		val addDecTimes: IndexedSeq[Double] = for (i <- 0 to numOps - 1) yield {
			val start = System.nanoTime
			qc.decryptAdd(addEncSet(i))
			val end = System.nanoTime
			(end - start) / tenPowerSix
		}

		
		/* ----- Results -------------- */
		
		val opeEncAvg = (opeEncTimes.foldLeft(0.0)(_ + _)) / numOps
		val detEncAvg = (detEncTimes.foldLeft(0.0)(_ + _)) / numOps
		val addEncAvg = (addEncTimes.foldLeft(0.0)(_ + _)) / numOps
		val opeDecAvg = (opeDecTimes.foldLeft(0.0)(_ + _)) / numOps
		val detDecAvg = (detDecTimes.foldLeft(0.0)(_ + _)) / numOps
		val addDecAvg = (addDecTimes.foldLeft(0.0)(_ + _)) / numOps

		println(s"DET -> Average Encryption Time: $detEncAvg ms; Throughput: ${calcThroughput(wordLength, detEncAvg)} kB/s")
		println(s"OPE -> Average Encryption Time: $opeEncAvg ms; Throughput: ${calcThroughput(4, opeEncAvg)} kB/s")
		println(s"ADD -> Average Encryption Time: $addEncAvg ms; Throughput: ${calcThroughput(4, addEncAvg)} kB/s")
		println(s"DET -> Average Decryption Time: $detDecAvg ms; Throughput: ${calcThroughput(wordLength, detDecAvg)} kB/s")
		println(s"OPE -> Average Decryption Time: $opeDecAvg ms; Throughput: ${calcThroughput(4, opeDecAvg)} kB/s")
		println(s"ADD -> Average Decryption Time: $addDecAvg ms; Throughput: ${calcThroughput(4, addDecAvg)} kB/s")

	}
	
}
