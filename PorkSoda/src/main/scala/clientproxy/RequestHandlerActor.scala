package clientproxy

/*
 * The RequestHandlerActor handles requests for SQL/MIE/SQL+MIE operations.
 * */


import akka.actor.{Actor, ActorSystem, ActorRef, Props}
import akka.stream.ActorAttributes
import akka.stream.scaladsl.{Source, Sink}
import java.sql.{ResultSet, SQLException}
import akka.util.Timeout

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

import common._
import queryProcessing.QueryProcessor
import queryProcessing.ResultSetRowIterator



class RequestHandlerActor extends Actor {

  val timeout: Timeout = 360.seconds

	implicit val executionContext = context.system.dispatchers.lookup("client-proxy-blocking-dispatcher")
  var processor: QueryProcessor = null


  override def postStop() {
    if (processor != null)
      processor.close()
    processor = null
  }


  def receive = {
  
    /*
     * start the SQL connection to the required database
     * TODO: provide a return message for both success and failure
     * */
    case SQLConnect(db, user, password) =>
      try {
        processor = new QueryProcessor(db, user, password)
        sender() ! ConnectResponse(true)
      }
      catch {
        case e: SQLException => sender() ! ConnectResponse(false)
      }


    /*
     *
     * */
    case SQLSetAutoCommit(autoCommit) =>
      try {
        processor.setAutoCommit(autoCommit)
        sender() ! SQLSuccess(0)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLSetTransactionIsolation(level) =>
      try {
        processor.setTransactionIsolation(level)
        sender() ! SQLSuccess(0)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLCommit =>
      try {
        processor.commit
        sender() ! SQLSuccess(0)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLRollback =>
      try {
        processor.rollback
        sender() ! SQLSuccess(0)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLExecuteUpdate(query) =>
      try {
        val updateResult = processor.executeUpdate(query)
        sender() ! SQLSuccess(updateResult)
      }
      catch {
        case e: SQLException => sender() ! SQLFailure(e.toString)
      }


    /*
     *
     * */
    case SQLExecuteQuery(query, nCols) =>
      try {
        val rsIterator = processor.executeQuery(query)
				val source = Source.fromIterator(() => rsIterator)
        sender() ! Left(source)
      }
      catch {
        case e: SQLException => sender() ! Right(SQLFailure(e.toString))
      }


    /*
     *
     * */
    case MIEIndex =>
      sender() ! processor.indexImages


    /*
     *
     * */
    case MIEAddImage (id: Int, bytes: Array[Byte]) =>
      sender() ! processor.addImage(id, bytes)


    /*
     *
     * */
    case MIESearchImage (bytes: Array[Byte]) =>
      sender() ! processor.searchImage(bytes)
	

	/*
	 *
	 * */
	case MIEGetImage (id: Int) =>
	  sender() ! processor.getImage(id)


	/*
	 *
	 * */
	case MultimodalQuery (sqlQuery: SQLExecuteQuery, conjunction: String, imageField: String, mieSearch: MIESearchImage) =>
	  try {
		val rsIterator = processor.multimodalQuery(sqlQuery.query, conjunction, imageField, mieSearch.bytes)
		sender() ! Left(Source.fromIterator(() => rsIterator))
	  }
	  catch {
		case e: SQLException => sender() ! Right(SQLFailure(e.toString))
	  }

  
  }
  
}
