package clientproxy

/*
 * Main defines the REST API through an Akka Http route.
 * Request data (such as a query) is represented in JSON format in its entity
 * A single ManagerActor is issued the requests. For a new client connection, it
 * assigns an ID and a newly spawned RequestHandlerActor instance. Subsequent requests
 * such as queries are then forwarded to the client's assigned RequestHandlerActor.
 * */


import akka.actor.{Actor, ActorSystem, ActorRef, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.stream.ActorMaterializer
import akka.pattern.ask
import akka.stream.scaladsl.{Source, Flow, Sink}
import scala.io.StdIn
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import akka.util.Timeout
import akka.util.ByteString

import common._
import common.ProxyJsonSupport._

import java.security.Security
import org.bouncycastle.jce.provider.BouncyCastleProvider
import mie.crypto.MIEProvider


object Main {

  import queryProcessing.Schema


  def main (args: Array[String]) {
    
    //println (System.getProperties)
    Security.addProvider(new BouncyCastleProvider)
    Security.addProvider(new MIEProvider)


    // proxy will bind to this address and port
    var address = "localhost"
    var port = 5150
    val schemasFile = "databaseSchemas"

    if (args.size == 2) {
      address = args(0)
      port = args(1).toInt
    }
    else if (args.size > 1) {
      println("usage: clientproxy.Main [address port]\nExample: clientproxy.Main localhost 5150")
      System.exit(1)
    }


    try {
      print("Loading schemas... ")
      Schema.loadSchema(schemasFile)
      println("Loaded.")
    }
    catch {
      case e: Throwable => println("No schemas file found. New structure created.")
    }

    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()

    // dedicated dispatcher for blocking operations (asks to actors, futures, etc)
    implicit val executionContext = system.dispatchers.lookup("client-proxy-blocking-dispatcher")

    //implicit val timeout: Timeout = 60.seconds
    implicit val longTimeout: Timeout = 6.minutes // for REEEEEALLY long operations

    val start = ByteString.empty
    val sep = ByteString("\n")
    val end = ByteString.empty
    implicit val jsonStreamingSupport: JsonEntityStreamingSupport = EntityStreamingSupport.json()
      .withFramingRenderer(Flow[ByteString].intersperse(start, sep, end))

    // takes care of client session management and request forwarding
    val manager = system.actorOf(Props[ManagerActor].withDispatcher("client-proxy-blocking-dispatcher"), "manager")

    val route =
      path("connect") {
        post {
          entity(as[SQLConnect]) { request =>
            val future = manager ? Connect(request)
            val result = Await.result(future, longTimeout.duration).asInstanceOf[Option[Int]]
            result match {
              case Some(id) =>
                println(s"Client #$id connected." )
                complete(StatusCodes.OK, s"$id")
              case None =>
                complete(StatusCodes.InternalServerError)
            }
          }
        }
      } ~
      path("disconnect") {
        get {
          parameter("id".as[Int]) { id =>
            manager ! Disconnect(id)
            println(s"Client #$id disconnected.")
            complete(StatusCodes.OK, "disconnected")
          }
        }
      } ~
      path("setAutoCommit") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[SQLSetAutoCommit]) { setAutoCommit =>
              val future = manager ? Forward(id, setAutoCommit)
              val futureResult = Await.result(future, longTimeout.duration).asInstanceOf[SQLResponse]
              futureResult match {
                case SQLSuccess(_) => complete(StatusCodes.OK)
                case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
              }
            }
          }
        }
      } ~
      path("setTransactionIsolation") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[SQLSetTransactionIsolation]) { transactionIsolation =>
              val future = manager ? Forward(id, transactionIsolation)
              val futureResult = Await.result(future, longTimeout.duration).asInstanceOf[SQLResponse]
              futureResult match {
                case SQLSuccess(_) => complete(StatusCodes.OK)
                case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
              }
            }
          }
        }
      } ~
      path("commit") {
        get {
          parameter("id".as[Int]) { id =>
            val future = manager ? Forward(id, SQLCommit)
            val futureResult = Await.result(future, longTimeout.duration).asInstanceOf[SQLResponse]
            futureResult match {
              case SQLSuccess(_) => complete(StatusCodes.OK)
              case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
            }
          }
        }
      } ~
      path("rollback") {
        get {
          parameter("id".as[Int]) { id =>
            val future = manager ? Forward(id, SQLRollback)
            val futureResult = Await.result(future, longTimeout.duration).asInstanceOf[SQLResponse]
            futureResult match {
              case SQLSuccess(_) => complete(StatusCodes.OK)
              case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
            }
          }
        }
      } ~
      path("executeUpdate") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[SQLExecuteUpdate]) { query =>
              val result = Future {
                val future = manager ? Forward(id, query)
                Await.result(future, longTimeout.duration).asInstanceOf[SQLResponse]
              }
              onSuccess(result) {
                case s: SQLSuccess => complete(s)
                case failure: SQLFailure => complete(StatusCodes.ImATeapot, failure)
              }
						}
					}
				}
      } ~
      path("executeQuery") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[SQLExecuteQuery]) { query =>
							val result = Future {
                val future = manager ? Forward(id, query)
                Await.result(future, longTimeout.duration).asInstanceOf[Either[Source[RSRow, _], SQLFailure]]
              }
              onSuccess(result) {
                case Left(source) => complete(source)
                case Right(failure) => complete(StatusCodes.ImATeapot, failure)
              }
            }
          }
        }
      } ~
      path("indexImages") {
        get {
          parameter("id".as[Int]) { id =>
            val future = manager ? Forward(id, MIEIndex)
            val futureResult = Await.result(future, longTimeout.duration).asInstanceOf[MIEResponse]
            futureResult match {
              case s: MIESuccess => complete(s)
              case failure: MIEFailure => complete(StatusCodes.ImATeapot, failure)
            }
          }
        }
      } ~
      path("addImage") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[MIEAddImage]) { request =>
              val future = manager ? Forward(id, request)
              val futureResult = Await.result(future, longTimeout.duration).asInstanceOf[MIEResponse]
              futureResult match {
                case s: MIESuccess => complete(s)
                case failure: MIEFailure => complete(StatusCodes.ImATeapot, failure)
              }
            }
          }
        }
      } ~
      path("searchImage") {
        post {
           parameter("id".as[Int]) { id =>
            entity(as[MIESearchImage]) { request =>
              val future = manager ? Forward(id, request)
              val futureResult = Await.result(future, longTimeout.duration).asInstanceOf[MIEData]
              futureResult match {
                case s: MIESearchResultArray => complete(s)
              }
            }
          }       
        }
      } ~
      path("getImage") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[MIEGetImage]) { request =>
              val future = manager ? Forward(id, request)
              val futureResult = Await.result(future, longTimeout.duration).asInstanceOf[MIEImage]
              futureResult match {
                case image: MIEImage => complete(image)
              }
            }
          }
				}
	  	} ~
      path("multimodalQuery") {
        post {
          parameter("id".as[Int]) { id =>
            entity(as[MultimodalQuery]) { query =>
            onSuccess(Future {
                val future = manager ? Forward(id, query)
                Await.result(future, longTimeout.duration).asInstanceOf[Either[Source[RSRow, _], SQLFailure]]
              }) { sourceFuture =>
                sourceFuture match {
                  case Left(source) => complete(source)
                  case Right(failure) => complete(StatusCodes.ImATeapot, failure)
                }
              }
            }
          }
        }
      }



    /*
     * Bind the proxy at http://address:port/
     * Trigger unbinding and termination if RETURN is pressed
     * */
    val bindingFuture = Http().bindAndHandle(route, address, port)
    println(s"Client Proxy online at http://$address:$port/\nPress RETURN to stop...")
    StdIn.readLine()

    try {
      Schema.saveSchema(schemasFile)
    }
    catch {
      case e: Throwable => println("ERROR: Could not write schemas file!")
    }

    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  
  }


}
